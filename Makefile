###################################
#        ICP Bludiste 2014        #
###################################


QMAKE=qmake
export QT_SELECT=qt5
LOGIN=xkrizv01
#LOGIN pro vytvoreni archivu


all: build


.PHONY: build clean doxygen pack clean-all run



build: src/Makefile
	make -C src
	cp src/server/server ./bludiste2014-server
	cp src/client/client ./bludiste2014
	cp src/client-cli/client-cli ./bludiste2014-cli


doxygen: src/Makefile
	make -C src clean
	doxygen


clean: src/Makefile
	make -C src distclean
	
clean-all: clean
	rm -f bludiste2014 bludiste2014-cli bludiste2014-server

pack: clean
	tar --exclude=icp.pro.user --exclude-vcs --exclude-backups -czf $(LOGIN).tar.gz src/ images/ maps/ Makefile README Doxyfile
	mkdir $(LOGIN)
	tar -xzf $(LOGIN).tar.gz -C $(LOGIN)
	tar --exclude=icp.pro.user --exclude-vcs --exclude-backups -czf $(LOGIN).tar.gz $(LOGIN)/*
	rm -r $(LOGIN)

src/Makefile:
	cd src/; $(QMAKE) icp.pro

#spusteni vseho
run:bludiste2014-server bludiste2014-cli bludiste2014
	./bludiste2014-server & 
	./bludiste2014 & 
	./bludiste2014-cli && killall bludiste2014-server

bludiste2014-server: build
bludiste2014: build
bludiste2014-cli: build



