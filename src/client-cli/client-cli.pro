#-------------------------------------------------
#
# Project created by QtCreator 2014-04-28T19:50:44
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = client-cli
CONFIG   += console
#CONFIG   -= app_bundle

TEMPLATE = app

include(../icp.pri)


SOURCES += main.cpp \
    gamewindow.cpp \
    init.cpp \
    lobbywindow.cpp \
    selectbox.cpp \
    newgamewindow.cpp \
    connectwindow.cpp
HEADERS += ../interface/interface.hpp \
    gamewindow.hpp \
    init.hpp \
    lobbywindow.hpp \
    selectbox.hpp \
    printmiddle.hpp \
    screen.hpp \
    newgamewindow.hpp \
    connectwindow.hpp

LIBS += -lncurses -lmenu -lform

LIBS += ../interface/libinterface.a    
LIBS += ../shared/libmaze.a
PRE_TARGETDEPS += ../shared/libmaze.a
PRE_TARGETDEPS += ../interface/libinterface.a
