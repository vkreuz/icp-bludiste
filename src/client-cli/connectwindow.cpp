/**
 * @brief Okno s dialogem pro připojení k serveru
 * @author Vítězlsav Kříž - xkrizv01 
 * @file connectwindow.cpp
 */

#include "connectwindow.hpp"
#include "lobbywindow.hpp"
#include "printmiddle.hpp"


/**
 * @brief Inicializuje připojovací okno, volá se z konstruktorů
 */
void ConnectWindow::init()
{
  
  // Výpočet velikosti prvlů v závislosti na velikosti okna
  // Vytvoření prvků
  fields[0] = new_field(1, COLS-20, 2, 1, 0, 0);
  fields[1] = new_field(1, 7, 5,  1 + (COLS-20-7)/2, 0, 0);
  fields[2] = new_field(1, COLS-20, 7, 1, 0, 0);
  fields[3] = new_field(1, COLS-20, 9, 1, 0, 0);
  fields[4] = nullptr;

  // Nastavení parametrů
  set_field_back(fields[0], A_UNDERLINE); 	
  field_opts_off(fields[0], O_AUTOSKIP);  	
  set_field_buffer(fields[0],0,"localhost");
  
  set_field_back(fields[1], A_UNDERLINE); 	
  field_opts_off(fields[1], O_AUTOSKIP);  
  set_field_buffer(fields[1],0,"45678");
  
  field_opts_off(fields[2], O_EDIT);
  set_field_just(fields[2],JUSTIFY_CENTER);
  set_field_buffer(fields[2],0,"Pripojit");
  
  field_opts_off(fields[3], O_EDIT);
  set_field_just(fields[3],JUSTIFY_CENTER);
  set_field_buffer(fields[3],0,"Ukoncit");
  
  
  my_form = new_form(fields);
  form_opts_off(my_form,O_BS_OVERLOAD);
  
  int rows,cols;
  scale_form(my_form, &rows, &cols);
  
  //Vytvoření nového okna
  my_form_win = newwin(rows + 4, cols+4, (LINES-rows-4)/2, (COLS-cols-4)/2);
  my_form_win_sub = derwin(my_form_win, rows, cols, 2, 2);
  
  set_form_win(my_form,my_form_win);
  set_form_sub(my_form,my_form_win_sub);
  
  //Vykreslení rámečku
  box(my_form_win,0,0);
  keypad(my_form_win,true);
}

/**
 * @brief Základní konstruktor
 */
ConnectWindow::ConnectWindow()
{
  init();
  title="Pripojeni k hernimu serveru";
}

/**
 * @brief Konstrukor který umožňuje nastavení titulku
 * @param msg Titulek okna
 */
ConnectWindow::ConnectWindow(std::string msg)
{
  init();
  title=msg;
}

ConnectWindow::~ConnectWindow()
{
  curs_set(0);
  free_form(my_form);
  delwin(my_form_win_sub);
  delwin(my_form_win);
  for(int i=0;fields[i]!=nullptr;++i)
    free_field(fields[i]);
}

/**
 * @brief Zobrazí okno
 *
 * Zobrzí okno s připojením,
 * políčka pro zadání serveru a portu, tlačítka Připojit a Ukončit
 * @see Screen::show
 * @return Ukazatel na @ref LobbyWindow nebo nullptr pro exit
 */
Screen *ConnectWindow::show()
{
  curs_set(1);
  post_form(my_form);
  wrefresh(my_form_win);
  
  printMiddle(my_form_win,1,1,getmaxx(my_form_win)-2,title.c_str());
  printMiddle(my_form_win,3,1,getmaxx(my_form_win)-2,"Adresa serveru");
  printMiddle(my_form_win,6,1,getmaxx(my_form_win)-2,"Port");
  
  wrefresh(my_form_win);
  set_current_field(my_form,fields[0]);
  form_driver(my_form, REQ_END_LINE);
  int ch;
  while((ch=wgetch(my_form_win))!='q'){
    switch(ch){  
      case KEY_DOWN:
        form_driver(my_form, REQ_NEXT_FIELD);
        form_driver(my_form, REQ_END_LINE);
        break;
      case KEY_UP:
        /* Go to previous field */
        form_driver(my_form, REQ_PREV_FIELD);
        form_driver(my_form, REQ_END_LINE);
        break;
      case KEY_ENTER:
      case '\n':
        if(my_form->current==fields[2]){
          std::string host (field_buffer(fields[0],0));
          host.erase(host.begin()+host.find(' '),host.end());
          
          try{
            
            Interface::getInstance()->init(host,std::atoi(field_buffer(fields[1],0)));
          }
          catch(const char *e){
            return new ConnectWindow("CHYBA: Nelze pripojit k serveru. Zkuste znovu.");
          }
          
          return new LobbyWindow;
        }
        if(my_form->current==fields[3]){
          return nullptr;
        }
        break;
      case KEY_BACKSPACE:
        form_driver(my_form,REQ_DEL_PREV);
        break;
      default:
        form_driver(my_form, ch);
        break;
    }
  }
  return nullptr;
}




