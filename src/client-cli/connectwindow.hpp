/**
 * @brief Okno s dialogem pro připojení k serveru
 * 
 * Využívá ncurses a form.
 * @author Vítězlsav Kříž - xkrizv01 
 * @file connectwindow.hpp
 */
#ifndef CONNECTWINDOW_HPP
#define CONNECTWINDOW_HPP

#include "screen.hpp"
#include <form.h>
#include <string>



/**
 * @brief Okno s dialogem pro připojení k serveru
 * 
 * Zajišťuje i samotné připojení k serveru.
 * Po úspěšném připojení se přejde na okno @ref LobbyWindow,
 * při neúspěšné se přejde znuvu na toto okno.
 */
class ConnectWindow : public Screen
{
  FORM * my_form; ///< Formulář
  FIELD * fields[5]; ///< Políčka formuláře
  WINDOW *my_form_win; ///<Okno s rámečkem
  WINDOW *my_form_win_sub; ///<Okno s formulářem
  std::string title; ///< Titulek okna
  void init();
public:
  ConnectWindow();
  ConnectWindow(std::string msg);
  Screen * show();
  ~ConnectWindow();
};

#endif // CONNECTWINDOW_HPP
