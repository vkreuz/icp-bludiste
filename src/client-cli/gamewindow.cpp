/**
 * @brief Obrazovka s hrou
 * 
 * Využívá ncurses
 * Zajišťuje celé herní okno a ovládání.
 * Používá vlákna std::thread
 * @author Vítězlsav Kříž - xkrizv01 
 * @file gamewindow.cpp
 */
#include "gamewindow.hpp"

#include "lobbywindow.hpp"
#include "printmiddle.hpp"

/**
 * @brief Konstruktor
 * 
 * Spouští další vlákno pro řízení vstupů
 */
GameWindow::GameWindow()
{
  thread_input = std::thread(&GameWindow::processInput,this);
}

GameWindow::~GameWindow()
{
  delwin(game);
  delwin(term);
  delwin(resultwin);
}


/**
 * @brief Zobrazí herní obrazovku
 * 
 * Ve smyčce čte data od serveru a vykresluje obrazovku.
 * O vstup od uživatel se se stará metoda @ref proccesInput
 * @see Screen::show
 * @return Ukazatel na LobbyWindow
 */
Screen *GameWindow::show()
{
  

  while( Interface::getInstance()->game()  )
  {
      //zmena rozmeru terminalu
      if(old_rows!=LINES || old_cols!=COLS){
        rows_game=LINES;
        cols_game=COLS-20;
        old_rows=LINES;
        old_cols=COLS;
        if(rows_game>Interface::getInstance()->get_current_map()->Rows())
          rows_game = Interface::getInstance()->get_current_map()->Rows();
        if(cols_game>Interface::getInstance()->get_current_map()->Cols())
          cols_game = Interface::getInstance()->get_current_map()->Cols();
        rows_term=LINES;
        cols_term=20;
        bkgd(COLOR_PAIR(C_BCKG));
        delwin(game);
        game=newwin(rows_game, cols_game, (LINES-rows_game)/2 ,(COLS-20-cols_game)/2);
        delwin(term);
        term=newwin(rows_term,cols_term,0,COLS-cols_term);
        wbkgd(game,COLOR_PAIR(C_BCKG));  
        refresh();        
      }

      
      //wrefresh(game);
      printMap(game,rows_game,cols_game);
      cmdHistory(term,rows_term,cols_term);
      wrefresh(game);
      wrefresh(term);
  }
  
  stopThread();
  showResult();
  thread_input.join();
  
  return new LobbyWindow;
  
  
}

/**
 * @brief GameWindow::processInput Rizeni vstupu od uzivatele
 * 
 * Spusti se ve vlastnim vlakne, na zaklade vstupu od uzivatele posila prikazy
 */
void GameWindow::processInput()
{
  int ch;
  Interface::getInstance()->command("stop");
  while((ch=getch()) != 'q')
  {
      if(getStop())
        return;
      switch(ch){
        case KEY_LEFT:
          Interface::getInstance()->command("left");
          break;
        case KEY_RIGHT:
          Interface::getInstance()->command("right");
          break;
        case KEY_DOWN:
          Interface::getInstance()->command("stop");
          break;
        case KEY_UP:
          Interface::getInstance()->command("go");
          break;
        case ' ':
          Interface::getInstance()->command("take");
          Interface::getInstance()->command("open");
          break;
        case 'l':
          Interface::getInstance()->command("left");
          break;
        case 'r':
          Interface::getInstance()->command("right");
          break;
        case 's':
          Interface::getInstance()->command("stop");
          break;
        case 'g':
          Interface::getInstance()->command("go");
          break;
        case 't':
          Interface::getInstance()->command("take");
          break;
        case 'o':
          Interface::getInstance()->command("open");
          break;
          
     }
  }
  Interface::getInstance()->send("199 ");
  ch=getch();
  
  
}

/**
 * @brief GameWindow::printMap Vytiskne mapu do okna
 * @param w Ukazatel na okno
 * @param rows Pocet radku okna
 * @param cols Pocet sloupcu okna
 */
void GameWindow::printMap(WINDOW *w, int rows, int cols)
{
  
  Maze & map=*Interface::getInstance()->get_current_map();
  player_info* curr_player;
  int start_row,start_col,end_row,end_col;
  
  curr_player=Interface::getInstance()->get_local_player();
  
  //vykresluje pokud je akutalni hrac (neni kdyz je zabit)
  if(curr_player != nullptr){
    
    start_row=curr_player->row-rows/2;
    end_row=curr_player->row+rows/2+rows%2;
    
    start_col=curr_player->col-cols/2;
    end_col=curr_player->col+cols/2+cols%2;
    
  } else {
    start_row=start_col=0;
    end_row=rows;
    end_col=cols;
  }
  
  // omezujici podminky, kdyz by se melo vykreslovat mimo mapu
  if(start_row<0){
    end_row+=-start_row;
    start_row=0;
  }
  
  if(start_col<0){
    end_col+=-start_col;
    start_col=0;
  }
  
  if(end_row>map.Rows()){
    start_row-=end_row-map.Rows();
    end_row=map.Rows();
    start_row=start_row<0?0:start_row;
  }
  
  if(end_col>map.Cols()){
    start_col-=end_col-map.Cols();
    end_col=map.Cols();
    start_col=start_col<0?0:start_col;
  }
  
  //vykresluje se v rozsahu start_col..end_col a start_row..end_row
  
  //vykreslovani obsahu
  for(int r=start_row;r<end_row;++r){
    for(int c=start_col;c<end_col;++c){
      chtype ch;
      switch(map.getField(r,c).get_type()){
        case Object::GATE:
          if(map.getField(r,c).is_open()){
            ch=CH_GATE_O;
          } else {
            ch=CH_GATE_C;
          }
          break;
        case Object::KEY:
          ch=CH_KEY;
          break;
        case Object::GOAL:
          ch=CH_GOAL;
          break;
        case Object::WALL:
          /*ch=wallChar(map.getField(r-1,c).get_type()==Object::WALL,
                      map.getField(r,c+1).get_type()==Object::WALL,
                      map.getField(r+1,c).get_type()==Object::WALL,
                      map.getField(r,c-1).get_type()==Object::WALL);
          ch|=COLOR_PAIR(C_OTHER);
          */
          ch=CH_WALL;
          break;
        default:
          ch='.' | COLOR_PAIR(C_EMTPY);
      }
      //vykresleni znaku na pozici
      mvwaddch(w,r-start_row,c-start_col,ch);
      
        
    }
  }
  
  //vykreslovani hracu
  
  for(int i = 0; i < MAX_PLAYERS; i++)
  {
      curr_player = Interface::getInstance()->get_player(i);
      if(curr_player->id==-1){
        continue;
      }
      int r= curr_player->row; 
      int c= curr_player->col;
      //pouze ti kteri jsou v okne
      if(r>=start_row && r <end_row && c>=start_col && c<end_col)
        mvwaddch(w,r-start_row,c-start_col,playerIcon(curr_player->dir) | COLOR_PAIR(curr_player->id%4+1));
  }
  
  //vykreslovani hlidacu
  std::vector<player_info> hlidaci(*Interface::getInstance()->getGuards());
  for (auto it = std::begin ( hlidaci ); it != std::end ( hlidaci ); ++it ) 
  {
    int r= it->row; 
    int c= it->col;
    if(r>=start_row && r <end_row && c>=start_col && c<end_col)
      mvwaddch(w,r-start_row,c-start_col,playerIcon(it->dir) | A_BOLD | COLOR_PAIR(C_G));
  }
  
}




/**
 * @brief GameWindow::cmdHistory Zobrazuje okno s historii
 * 
 * 
 * @param w Ukazatel na okno k zobrazeni
 * @param rows Rozmery okna - radky
 * @param cols Rozmery okna - sloupce
 */
void GameWindow::cmdHistory(WINDOW *w, int rows, int cols)
{
  std::istringstream data ( Interface::getInstance()->response);
  std::string line;
  while(std::getline(data,line)){
    history.push_back(std::move(line));
    if(history.size()>(unsigned)rows){
      history.pop_front();
    }
  }
  Interface::getInstance()->response.clear();
  std::string space;
  for(int i=0;i<cols;++i)
    space.push_back(' ');
  for(unsigned i=0;i<history.size();++i){
    mvwaddstr(w,i,0,space.c_str());
    mvwaddstr(w,i,0,history[i].c_str());
  }
  
  
}

/**
 * @brief Generuje znak pro hrace/hlidace
 * 
 * na zaklade smeru
 * @param dir smer 0-N 1-E 2-S 3-W
 * @return Znak, ^,>,v,<
 */
chtype GameWindow::playerIcon(int dir)
{
  chtype c='P';
  switch(dir){
    case 0:
      c='^';
      break;
    case 1:
      c='>';
      break;
    case 2:
      c='v';
      break;
    case 3:
      c='<';
      break;
  }
  return c;
}


/**
 * @brief Zobrazí okno přes herní okno s výsledky.
 * 
 * Výsledky hry zobrazuje jako tabulku s hráči.
 * Okno se zobrazí uprostřed obrazovky
 */
void GameWindow::showResult()
{
  int width=30;
  int height=10;
  
  resultwin=newwin(height,width,(LINES-height)/2,(COLS-width)/2);
  box(resultwin,0,0);
  printMiddle(resultwin,1,1,width-2,"KONEC HRY");
  if(!Interface::getInstance()->players_data.empty())
    printMiddle(resultwin,2,1,width-2,"Cas hry: " + std::to_string(Interface::getInstance()->players_data.front().game_time) );
  
  const unsigned layout[5] = {3,2,6,6,6};
  int line = 3;
  int col=1;
  std::vector<std::string> cels;
  
  cels.push_back("win");
  cels.push_back("P");
  cels.push_back("kroky");
  cels.push_back("cas");
  cels.push_back("umrti");
  col=1;
  
  for(unsigned field=0;field<cels.size();++field){
    std::string & str=cels[field];
    if(str.length()>layout[field])
      str.erase(str.begin(),str.end()-layout[field]);
    mvwaddstr(resultwin,line,col+layout[field]-str.length(),str.c_str());
    col+=layout[field]+1;
  }
  line++;
  
  for(post_game_info &player:Interface::getInstance()->players_data){
    cels.clear();
    //vyhral?
    std::vector<int> &winners (Interface::getInstance()->winners);
    if(std::find(winners.begin(),winners.end(),player.ID)!=winners.end())
      cels.push_back("yes");
    else
      cels.push_back(" ");
    cels.push_back("P" + std::to_string(player.ID));
    cels.push_back(std::to_string(player.num_steps));
    cels.push_back(std::to_string(player.player_time));
    cels.push_back(std::to_string(player.deaths));
    col=1;
    for(unsigned field=0;field<cels.size();++field){
      std::string & str=cels[field];
      if(str.length()>layout[field])
        str.erase(str.begin(),str.end()-layout[field]);
      wattron(resultwin,COLOR_PAIR(player.ID%4+1));
      mvwaddstr(resultwin,line,col+layout[field]-str.length(),str.c_str());
      wattroff(resultwin,COLOR_PAIR(player.ID%4+1));
      col+=layout[field]+1;
    }
    line++;
  }
  col=0;
  for(unsigned field=0;field<cels.size();++field){
    col+=layout[field]+1;
    mvwvline(resultwin,3,col,0,line-2);  
  }
  
  
  
  wrefresh(resultwin);
}


/**
 * @brief GameWindow::wallChar Generuje znak zdi
 * 
 * Generuje znak zdi na zaklade sousednich zdi, pouziva ACS znaky (rohy,cary,tecka)
 * V programu nevyuzito, protoze nesplnilo ocekavani na vzhled
 * @param N Zed na severu
 * @param E Zed na vychode
 * @param S Zed na jihu
 * @param W Zed na zapade
 * @return ACS znak
 */
chtype GameWindow::wallChar(bool N, bool E, bool S, bool W)
{
  u_int8_t x=0;
  if(N)
    x|=1<<3;
  if(E)
    x|=1<<2;
  if(S)
    x|=1<<1;
  if(W)
    x|=1;
  switch(x){
    case 0x0:
      return ACS_CKBOARD;
    case 0x1:
    case 0x4:
    case 0x5:
      return ACS_HLINE;
    case 0x2:
    case 0x8:
    case 0xA:
      return ACS_VLINE;
    case 0x3:
      return ACS_URCORNER;
    case 0x6:
      return ACS_ULCORNER;
    case 0x7:
      return ACS_TTEE;
    case 0x9:
      return ACS_LRCORNER;
    case 0xB:
      return ACS_RTEE;
    case 0xC:
      return ACS_LLCORNER;
    case 0xD:
      return ACS_BTEE;
    case 0xE:
      return ACS_LTEE;
    case 0xF:
      return ACS_PLUS;
    default:
      return '#';
  }

}