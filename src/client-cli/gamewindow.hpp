/**
 * @brief Obrazovka s hrou
 * 
 * Využívá ncurses
 * Zajišťuje celé herní okno a ovládání.
 * Používá vlákna std::thread
 * @author Vítězlsav Kříž - xkrizv01 
 * @file gamewindow.hpp
 */
#ifndef GAMEWINDOW_HPP
#define GAMEWINDOW_HPP

#include <ncurses.h>
#include "../interface/interface.hpp"
#include "init.hpp"
#include "screen.hpp"
#include <deque>
#include <thread>
#include <mutex>

/**
 * @brief Hlavní obrazovka s hrou
 * 
 * Zajišťuje ovládání zasílání příkazu a vykreslování bludiště.
 */
class GameWindow: public Screen
{
  /** Fronta obsahujici historii uspesnosti prikazu + zabiti hracu */
  std::deque<std::string> history;
  
  chtype wallChar(bool N,bool E,bool S, bool W);
  void processInput();
  void printMap(WINDOW * w,int rows, int cols);
  void cmdHistory(WINDOW *w, int rows, int cols);
  chtype playerIcon(int dir);
  void showResult();
  
  std::thread thread_input; ///< Vlákno pro zpracování vstupů z klávesnice
  std::mutex stop_lock;     ///< Zámek pro přístup k proměnné @ref stop
  bool stop=false;          ///< Promněná pro ukončení vlákna, true je příkaz pro ukončení
  inline void stopThread();
  inline bool getStop();    
  
  
  int rows_game=0; ///< Počet řádků okna kde se zobrazuje bludiště
  int cols_game=0; ///< Počet sloupců okna kde se zobrazuje bludiště
  int cols_term=0; ///< Počet sloupců okna s úspěšností příkazů
  int rows_term=0; ///< Počet řádků okna s úspěšností příkazů
  int old_rows=0;  ///< Rozměr terminálu - počet řádků
  int old_cols=0;  ///< Rozměr terminálu - počet sloupců
  WINDOW *game=nullptr; ///< Okno s hrou
  WINDOW *term=nullptr; ///< Okno s úspěšností příkazů
  WINDOW *resultwin=nullptr; ///< Okno pro zobrazení výsledků na konci hry
public:
  GameWindow();
  ~GameWindow();
  Screen*  show();
};

/**
 * @brief Zastaví vlákno thread_input
 * 
 * Způsobí doběhnutí vlákna thread_input
 */
void GameWindow::stopThread(){
  stop_lock.lock();
  stop=true;
  stop_lock.unlock();
}

/**
 * @brief Získá hodnotu @ref stop
 * 
 * Na základě této hodnotyse vlákno rozhoduje zda skoncit
 * @return  true = stop @ref stop
 */
bool GameWindow::getStop(){
  bool ret;
  stop_lock.lock();
  ret=stop;
  stop_lock.unlock();
  return ret;
}

#endif // GAMEWINDOW_HPP
