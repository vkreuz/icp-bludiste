/**
 * @brief Inicializace barev
 * @author Vítězlsav Kříž - xkrizv01 
 * @file init.cpp
 */
#include "init.hpp"


void init_pairs_of_colors(){
  init_pair(C_P1,COLOR_GREEN,COLOR_BLACK);
  init_pair(C_P2,COLOR_BLUE,COLOR_BLACK);
  init_pair(C_P3,COLOR_MAGENTA,COLOR_BLACK);
  init_pair(C_P4,COLOR_CYAN,COLOR_BLACK);
  init_pair(C_G,COLOR_RED,COLOR_BLACK);//HLIDAC
  init_pair(C_WALL,COLOR_WHITE,COLOR_WHITE );//WALL
  init_pair(C_EMTPY,COLOR_BLACK,COLOR_BLACK);//EMPTY
  init_pair(C_OTHER,COLOR_WHITE,COLOR_BLACK);//OTHER
  init_pair(C_BCKG,COLOR_YELLOW,COLOR_BLUE);
}
