/**
 * @brief Inicializační konstatny pro nastavení barev ncusres
 * 
 * Definuje makra pro čísla barev
 * @author Vítězlsav Kříž - xkrizv01 
 * @file init.hpp
 */

#ifndef INIT_HPP
#define INIT_HPP

#include "ncurses.h"

#define C_P1 1
#define C_P2 2
#define C_P3 3
#define C_P4 4
#define C_G 5
#define C_WALL 7
#define C_EMTPY 8
#define C_OTHER 9
#define C_BCKG 10
#define C_MENU 0
#define C_MENU_ACTIVE 1


#define CH_GOAL '@' | COLOR_PAIR(C_OTHER)
#define CH_GATE_O 'O' | COLOR_PAIR(C_OTHER)
#define CH_GATE_C ACS_CKBOARD | COLOR_PAIR(C_OTHER)
#define CH_KEY  '+' | COLOR_PAIR(C_G)
#define CH_WALL ' ' | COLOR_PAIR(C_WALL);


/**
 * @brief Inicializace barevných páru
 * 
 * Inicializuje barevné páry všech definovaných konstant
 */
void init_pairs_of_colors();

#endif // INIT_HPP
