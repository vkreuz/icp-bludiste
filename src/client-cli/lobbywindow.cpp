/**
 * @brief Okno s obrazovkou pro výběr hry
 * 
 * Využívá ncurses a menu pres @ref SelectBox
 * @author Vítězlsav Kříž - xkrizv01 
 * @file lobbywindow.cpp
 */
#include "lobbywindow.hpp"
#include "../interface/interface.hpp"
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <map>



#include "init.hpp"
#include "printmiddle.hpp"
#include "gamewindow.hpp"
#include "newgamewindow.hpp"

/**
 * @brief Vykresli obsah do okna s informacemi o hre
 * @param game Struktura s inforacemi o hre
 * @param w Vykreslovaci okno
 */
void LobbyWindow::drawGameInfo(Info &game,WINDOW *w)
{
  int cols=getmaxx(w);
  std::string title ("Informace o hre");
  box(w, 0, 0);
  printMiddle(w,1,1,cols-1,title);
  mvwaddch(w, 2, 0, ACS_LTEE);
  mvwhline(w, 2, 1, ACS_HLINE, cols-2);
  mvwaddch(w, 2, cols-1, ACS_RTEE);
  
  std::string str;
  
  str="Jmeno hry: "+game.get_name();
  str.erase(str.begin()+cols-2,str.end());
  mvwaddstr(w,3,1,str.c_str());
  
  str="Pocet hracu: "+std::to_string(game.get_num());
  str.erase(str.begin()+cols-2,str.end());
  mvwaddstr(w,4,1,str.c_str());
  wrefresh(w);
}


LobbyWindow::~LobbyWindow()
{
  delwin(win_info);
}



/**
 * @brief Zobrazí obrazovku, zajiš%tuje řízení
 * @see Screen::show
 * 
 * Zajišťuje ovládání šipkama po ovládacích prvcích. Po zvolení připojit
 * se připojí ke hře a vrátí ukazatel na vytvořenou obrazvku @ref GameWindow .
 * Při zvolení založit hru se přejde na obrazovku @ref NewGameWindow
 * @return Ukazatel na @ref GameWindow nebo @ref NewGameWindow nebo nullptr
 */
Screen *LobbyWindow::show()
{

  box_games.show();
  box_menu.show();
  box_games.enter();
  
  if(box_games.selected()>=0){
    drawGameInfo(games[box_games.selected()],win_info);  
  }
  
  int c;
  bool state=0;
  
  while((c = getch()) != 'q'){       
    
    switch(c){
      case KEY_DOWN:        
        if(state==0)
          box_games.down();
        if(state==1)
          box_menu.down();
        break;
      case KEY_UP:
        if(state==0)
          box_games.up();
        if(state==1)
          box_menu.up();
        break;
      case KEY_RIGHT:
        state=1;
        box_games.leave();
        box_menu.enter();
        break;
      case KEY_LEFT:
        state=0;
        box_games.enter();
        box_menu.leave();
        break;
      case KEY_ENTER:
      case '\n':
        if(box_menu.selected()==0){
          try{
          Interface::getInstance()->game_init(false,box_games.selected());
          }
          catch(const char *e){
            return new LobbyWindow;
          }

          return new GameWindow;
        } else
        if (box_menu.selected()==1){
          return new NewGameWindow;
        }
        else
        if (box_menu.selected()==2){
          return nullptr; //quit;
        }
    }
    
    
    int n = box_games.selected();
    if(n>=0){
      drawGameInfo(games[n],win_info);  
    }   
  }
  
  return nullptr;
  
}
