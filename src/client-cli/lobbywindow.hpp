/**
 * @brief Okno s obrazovkou pro výběr hry
 * 
 * Využívá ncurses a menu pres @ref SelectBox
 * @author Vítězlsav Kříž - xkrizv01 
 * @file lobbywindow.hpp
 */
#ifndef LOBBYWINDOW_HPP
#define LOBBYWINDOW_HPP
#include <menu.h>
#include "../interface/interface.hpp"
#include "selectbox.hpp"

#include "screen.hpp"


/**
 * @brief Okno pro připojení do hry
 * 
 * Zobrazuje rozehrané hry na serveru, umožňuje se k nim připojit
 * Následující obrazovka je GameWindow, nebo NewGameWindow nebo nullptr = exit
 */
class LobbyWindow: public Screen
{
  WINDOW * win_info; ///< Okno s inforamcemi o hre
  std::map<int,Info> &games; ///< Nactene hry ze serveru
  SelectBox box_games; ///< SelectBox s hrami
  SelectBox box_menu;  ///< Menu s tlacitky
  void drawGameInfo(Info &game,WINDOW *w);
  
public:
  //Vykresleni na spravne pozice
  LobbyWindow(): games(Interface::getInstance()->getGames2()),
                 box_games(LINES-4,COLS/2-8,2,4,"Rozehrane hry"),
                 box_menu(5,20,LINES-7,COLS/2+(COLS/2-20)/2)
  {
    win_info=newwin(10,COLS/2-8,2,COLS/2+4); 
    for(auto x:games){ 
      box_games.addItem(x.first,x.second.get_name());
    }
    box_menu.addItem(0,"Pripojit").addItem(1,"Nova hra").addItem(2,"Ukoncit");
  }
  ~LobbyWindow();
  Screen* show();
};

#endif // LOBBYWINDOW_HPP
