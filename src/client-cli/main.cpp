/**
 * @file client-cli/main.cpp
 * @brief Main funkce pro konzolového klienta
 * 
 * Klient využívá knihovnu ncurses a na ní závislé knihovny form a menu
 * @author Vítězlsav Kříž - xkrizv01
 */
#include <QCoreApplication>
#include <iostream>
#include <ncurses.h>
#include "../interface/interface.hpp"
#include "../shared/abstractplayer.hpp"
#include "gamewindow.hpp"
#include "lobbywindow.hpp"
#include "init.hpp"
#include "screen.hpp"
#include "connectwindow.hpp"


/**
 * @brief Hlavní funkce
 * 
 * Řídí pouze přepínání oken @ref Screen
 * @todo Nápověda 
 * @todo Příjmání parametrů příkazové řádky, pro připojení k serveru
 */
int main(int argc, char *argv[])
{
  
  QCoreApplication a(argc, argv);
  try{

    initscr(); // inicializace ncurses
    cbreak(); 
    keypad(stdscr, TRUE);
    curs_set(0);
    refresh();
  
    start_color();
    init_pairs_of_colors();
    noecho();
    bkgd(COLOR_PAIR(C_BCKG));
    
    refresh();
    
    
    //řízení přepínání oken
    Screen * actual;
    Screen * old;
    actual = new ConnectWindow;
    while(actual!=nullptr){
      old=actual;
      actual=old->show();
      delete old;
      clear();
      bkgd(COLOR_PAIR(C_BCKG));
      refresh();
    }

    endwin();
    
    return 0;
  }
  catch (char const * e){
    endwin();
    std::cerr <<e << "\n";
    return 0;
  }

  return a.exec();
}