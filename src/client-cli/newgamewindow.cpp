/**
 * @brief Okno s obrazovkou pro vytvoření nové hry
 * 
 * Využívá ncurses a menu pres @ref SelectBox
 * @author Vítězlsav Kříž - xkrizv01 
 * @file newgamewindow.cpp
 */
#include "newgamewindow.hpp"

#include "gamewindow.hpp"
#include "printmiddle.hpp"
#include "lobbywindow.hpp"
#include <cstdlib>

/**
 * @brief inicializuje správně formulářové okno
 */
void NewGameWindow::init()
{
  fields[0] = new_field(1, COLS/2-10,2 , 1, 0, 0);
  fields[1] = new_field(1, 7, 5,  1 + (COLS/2-10-7)/2, 0, 0);
  fields[2] = new_field(1, COLS/2-10, 7,  1, 0, 0);
  fields[3] = new_field(1, COLS/2-10, 9,  1, 0, 0);
  fields[4] = nullptr;
  
  set_field_back(fields[0], A_UNDERLINE); 	
  field_opts_off(fields[0], O_AUTOSKIP);  	
  
  field_opts_off(fields[1], O_AUTOSKIP);
  set_field_buffer(fields[1],0, std::string("< "+std::to_string(delay/10)+"."+std::to_string(delay%10)+" >").c_str());
  
  
  field_opts_off(fields[2], O_EDIT);
  set_field_just(fields[2],JUSTIFY_CENTER);
  set_field_buffer(fields[2],0,"Zalozit hru");
  
  
  field_opts_off(fields[3], O_EDIT);
  set_field_just(fields[3],JUSTIFY_CENTER);
  set_field_buffer(fields[3],0,"Zpet");
  
  
  my_form = new_form(fields);
  form_opts_off(my_form,O_BS_OVERLOAD);
  
  int rows,cols;
  scale_form(my_form, &rows, &cols);
  
  
  my_form_win = newwin(rows + 4, cols+4, 2, COLS/2);
  my_form_win_sub = derwin(my_form_win, rows, cols, 2, 2);
  
  set_form_win(my_form,my_form_win);
  set_form_sub(my_form,my_form_win_sub);
  
  box(my_form_win,0,0);
  keypad(my_form_win,true);
}

NewGameWindow::~NewGameWindow()
{
  curs_set(0);
  free_form(my_form);
  delwin(my_form_win_sub);
  delwin(my_form_win);
  for(int i=0;fields[i]!=nullptr;++i)
    free_field(fields[i]);
}

/**
 * @brief Zobrazí okno
 * @see Screen
 * @return ukazatel na GameWindow nebo LobbyWindow
 */
Screen *NewGameWindow::show()
{
   box_maps.show();
   box_maps.enter();
  post_form(my_form);
  wrefresh(my_form_win);
      
  int state=0;
  
      
      printMiddle(my_form_win,3,1,getmaxx(my_form_win)-2,"Jmeno hry");
      printMiddle(my_form_win,6,1,getmaxx(my_form_win)-2,"Zpozdeni");
      wrefresh(my_form_win);
      set_current_field(my_form,fields[0]);
      form_driver(my_form, REQ_END_LINE);
      int ch;
      /* Loop through to get user requests */
      while((ch = wgetch(my_form_win)) != 'q')
      {	
        if (state==0) {
          switch(ch) {
            case KEY_DOWN:
              box_maps.down();
              break;
            case KEY_UP:
              box_maps.up();
              break;
            case KEY_ENTER:
            case '\n':
            case KEY_RIGHT:
              box_maps.leave();
              form_driver(my_form, REQ_END_LINE);
              state=1;
              
              break;
            default:
              break;
          }
        }else if(state==1){
          switch(ch){  
            case KEY_DOWN:
              /* Go to next field */
              form_driver(my_form, REQ_NEXT_FIELD);
              /* Go to the end of the present buffer */
              /* Leaves nicely at the last character */
              form_driver(my_form, REQ_END_LINE);
              break;
            case KEY_UP:
              /* Go to previous field */
              form_driver(my_form, REQ_PREV_FIELD);
              form_driver(my_form, REQ_END_LINE);
              break;
            case KEY_LEFT:
              if(my_form->current==fields[1]){
                if(delay>5)
                  delay--;
                set_field_buffer(fields[1],0, std::string("< "+std::to_string(delay/10)+"."+std::to_string(delay%10)+" >").c_str());
              } else {
                box_maps.enter();
                state=0;
              }
              break;
            case KEY_RIGHT:
              if(my_form->current==fields[1]){
                if(delay<50)
                  delay++;
                set_field_buffer(fields[1],0, std::string("< "+std::to_string(delay/10)+"."+std::to_string(delay%10)+" >").c_str());
              }
              break;
            case KEY_ENTER:
            case '\n':
              if(my_form->current==fields[2]){
                std::string name (field_buffer(fields[0],0));
                name.erase(name.begin()+name.find(' '),name.end());
                if(name.empty())
                  name="New Game";
                try{
                  Interface::getInstance()->game_init(true,box_maps.selected(),name.c_str(),delay/10.0);
                }
                catch(const char *e){
                }

                return new GameWindow;
              }
              if(my_form->current==fields[3]){
                return new LobbyWindow;
              }
              break;
            case KEY_BACKSPACE:
              form_driver(my_form,REQ_DEL_PREV);
              break;
            default:
              /* If this is a normal character, it gets */
              /* Printed				  */	
              form_driver(my_form, ch);
              break;
          }
        }
        if(my_form->current==fields[0] && state==1){
          curs_set(1);
        } else {
          curs_set(0);
        }
        for(int i=0;fields[i]!=nullptr;++i){
          set_field_fore(fields[i],A_NORMAL);
        }
        set_field_fore(my_form->current,A_BOLD);
        
      }

   return new LobbyWindow;
}
