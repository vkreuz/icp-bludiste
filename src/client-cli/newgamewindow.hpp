/**
 * @brief Okno s obrazovkou pro vytvoření nové hry
 * 
 * Využívá ncurses a menu pres @ref SelectBox
 * @author Vítězlsav Kříž - xkrizv01 
 * @file newgamewindow.hpp
 */
#ifndef NEWGAMEWINDOW_HPP
#define NEWGAMEWINDOW_HPP

#include "screen.hpp"
#include <form.h>
#include "selectbox.hpp"
#include "../interface/interface.hpp"


/**
 * @brief Obrazovka s okny pro vyvtoření nové hry
 * 
 * Zobrazuje seznam map, políčko pro zadání názvu hry,
 * nastavení časové prodlevy hry.
 * Tlačítka pro vyvtoření hry a pro navrácení na úvodní obrazovku (LobbyWindow)
 */
class NewGameWindow : public Screen
{
  FORM * my_form;    ///< Formulář s tlačítky a input boxy
  FIELD * fields[5]; ///< Políčka formuláře
  WINDOW *my_form_win; ///<Okno formuláře s rámečkem
  WINDOW *my_form_win_sub; ///<Vnitřní okno formuláře
  std::map<int,Info> &maps; ///<Dostupné mapy na serveru
  SelectBox box_maps; ///< SelectBox s dostupnými mapami
  int delay=5;  ///< Nastavené zpoždění pro hru, číslo je v desetinnách sekund tj 5=0.5s
  void init();
public:

  NewGameWindow(): maps(Interface::getInstance()->getMaps2()),
                   box_maps(LINES-4,COLS/2-8,2,4,"Dostupne mapy")
  {
    for(auto x:maps){ 
      box_maps.addItem(x.first,x.second.get_name());
    }
    
    init();
  }
  
  ~NewGameWindow();
  Screen *show();
};

#endif // NEWGAMEWINDOW_HPP
