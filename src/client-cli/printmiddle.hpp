/**
 * @brief Funkce pro vykreslení textu uprostřed na zadaných pozcích
 * @author Vítězlsav Kříž - xkrizv01 
 * @file printmiddle.hpp
 */

#ifndef PRINTMIDDLE_HPP
#define PRINTMIDDLE_HPP

#include <ncurses.h>
#include <string>

/**
 * @brief Vykreslí text uprostřed na zadané pozici
 * 
 * Text vykreslí do okna. Je nutné zadat šířku. Delší text než je šířka ořízne
 * @param win Ukazatel na okno kam se má vykreslit
 * @param row Pozice - Řádek
 * @param col Pozice - Sloupec
 * @param width Šířka pole kde se má vykreslit
 * @param str Text na vykreslení
 */
inline void printMiddle(WINDOW *win, int row, int col, unsigned width, std::string str)
{
  if(win == NULL)
    win = stdscr;
  
  
  if(str.length()>width)
    str.erase(str.begin()+width-1,str.end());
  
  mvwaddstr(win,row, col + (width-str.length())/2 ,str.c_str());
}

#endif // PRINTMIDDLE_HPP
