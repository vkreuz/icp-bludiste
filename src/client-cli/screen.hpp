/**
 * @brief Screen
 * @author Vítězlsav Kříž - xkrizv01
 * @file screen.hpp
 */

#ifndef SCREEN_HPP
#define SCREEN_HPP


/**
 * @interface Screen
 * @brief Rozhraní pro obrazovky
 * 
 * Slouží pro vykresleni celého terminálového okna
 */
class Screen
{
public:
  Screen(){}
  virtual ~Screen(){}
  /**
   * @brief Vykreslí celý terminál
   * 
   * Funkce musí:
   * - Vykreslit celý terminál
   * - Zajistit si celé řízení pro svůj běh
   *   - Reakci na klávesy
   *   - Data z @ref Interface
   * - Funkce musí vrátit následující obrazovku, nebo nullptr - pro konec porgramu
   *   -Tuto obrazovku musí vytvořit pomocí new, není zodpovědná za zrušení
   * @return Následující obrazvovka, případně nullptr pro exit
   */
  virtual Screen* show()=0;
};

#endif // SCREEN_HPP
