/**
 * @brief SelectBox v ncurses
 * 
 * Modul se Select boxem.
 * Jedna se o objektovy wrapper nad ncruses a menu
 * @author Vítězlsav Kříž - xkrizv01
 * @file selectbox.cpp
 */
#include "selectbox.hpp"
#include <cstdlib>
#include <iostream>

#include "printmiddle.hpp"


/**
 * @brief Vykreslí rámeček
 */
void SelectBox::drawBox()
{
  box(my_menu_win, 0, 0);
  if(!title.empty()){
    printMiddle(my_menu_win,1,1,cols-1,title);
    mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
    mvwhline(my_menu_win, 2, 1, ACS_HLINE, cols-2);
    mvwaddch(my_menu_win, 2, cols-1, ACS_RTEE);
  }
}

/**
 * @brief Konstruktor pro variantu s titulkem
 * @param r Počet řádků
 * @param c Počet sloupců
 * @param sr řádková pozice levého horního rohu
 * @param sc sloupcová pozice levého horního rohu
 * @param tit titulek okna
 */
SelectBox::SelectBox(int r, int c, int sr, int sc, std::string tit)
{
  rows=r;
  cols=c;
  start_row=sr;
  start_col=sc;
  title=tit;
  maxItems=rows-4;
  my_menu_win = newwin(rows, cols, start_row, start_col);
  my_menu_win_sub=derwin(my_menu_win, maxItems, cols-2, 3, 1);
  
  wattron(my_menu_win,COLOR_PAIR(color_dark));
  drawBox();
  wattroff(my_menu_win,COLOR_PAIR(color_dark));
  
}


/**
 * @brief Konstruktor pro variantu bez titulku
 * @param r Počet řádků
 * @param c Počet sloupců
 * @param sr řádková pozice levého horního rohu
 * @param sc sloupcová pozice levého horního rohu
 */
SelectBox::SelectBox(int r, int c, int sr, int sc)
{
  rows=r;
  cols=c;
  start_row=sr;
  start_col=sc;
  title="";
  maxItems=rows-2;
  my_menu_win = newwin(rows, cols, start_row, start_col);
  my_menu_win_sub=derwin(my_menu_win, maxItems, cols-2, 1, 1);

  wattron(my_menu_win,COLOR_PAIR(color_dark));
  drawBox();
  wattroff(my_menu_win,COLOR_PAIR(color_dark));
}

/**
 * @brief Zajistí potřebné uvolnění všech alokovaných položek
 */
SelectBox::~SelectBox()
{
  unpost_menu(my_menu);
    
  if(my_menu!=nullptr){
    free_menu(my_menu);
  }
  
  if(my_items!=nullptr){
    for(unsigned i=0;i<items_map.size();++i){
      free_item(my_items[i]);
    }
    free(my_items);
  }
  
  delwin(my_menu_win_sub);
  delwin(my_menu_win);

}

/**
 * @brief Zobrazí nastavený SelectBox
 * 
 * Je potřeba předem zavolat addItem pro přidání položek,
 * nelze přidávat za běhu, ani překreslovat
 */
void SelectBox::show()
{
  my_items= (ITEM **) std::calloc(items_map.size()+1,sizeof(ITEM*));
  for(unsigned i=0;i<index2map.size();++i){
    my_items[i] = new_item(items_map[index2map[i]].c_str(),"");
  }
  my_items[items_map.size()]=0;
  
  my_menu = new_menu((ITEM **)my_items);
  
  set_menu_win(my_menu, my_menu_win);
  set_menu_sub(my_menu, my_menu_win_sub);
  set_menu_format(my_menu,maxItems,1);

  set_menu_mark(my_menu, " * ");
  
  refresh();
  post_menu(my_menu);
  wrefresh(my_menu_win);
}
