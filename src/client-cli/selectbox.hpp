/**
 * @brief SelectBox v ncurses
 * 
 * Modul se Select boxem.
 * Jedna se o objektovy wrapper nad ncruses a menu
 * @author Vítězlsav Kříž - xkrizv01
 * @file selectbox.hpp
 */
#ifndef SELECTBOX_HPP
#define SELECTBOX_HPP

#include <curses.h>
#include <menu.h>
#include <string>
#include <map>
#include <vector>



/**
 * @brief SelectBox wrapper nad ncurses a menu
 * 
 * Umožnuje vykreslit na zadanách pozicích menu/selectbox.
 * Využívá menu a ncurses. 
 */
class SelectBox
{
  int start_row; ///< Řadková souřadnice levého horního rohu
  int start_col; ///< Sloupcová souřadnice levého horního rohu
  int rows; ///< Počet řádku
  int cols; ///< Počet sloupců
  
  int maxItems; ///< Maximální počet položek
  
  std::string title; ///< Nadpis okna
  std::map<int,std::string> items_map; ///< Prvky k vykresleni, relace [id,nazev]
  std::vector<int> index2map; ///< Mapování pořadí na id

  ITEM **my_items=0; ///< Položky menu
  
  MENU *my_menu=0;  ///<  Menu
  WINDOW *my_menu_win=0; ///<Okno s rámečkem
  WINDOW *my_menu_win_sub=0;///< Okno s menu

  int color_active=1; ///< barva pro aktivní okno ID pro COLOR_PAIR
  int color_dark=0;   ///< barva pro opuštěné ID pro COLOR_PAIR
  
  void drawBox(); 
  
public:
  SelectBox(int r,int c,int sr,int sc,std::string tit);
  SelectBox(int r,int c,int sr,int sc);
  SelectBox(){}
  ~SelectBox();
  inline SelectBox &addItem(int index,std::string str);
  void show();
  inline void up();
  inline void down();
  inline int selected();
  inline void leave();
  inline void enter();
  void setColors(int active,int dark) {color_active=active; color_dark=dark;}
};


/**
 * @brief Přídá položku k vykresleni
 * 
 * Je nutné volat před vykreslením, neumožňuje dynamicky přidávat položky
 * @param index ID položky
 * @param str Text položky
 * @return Vrací referenci na svůj objekt, umožňuje zřetězení
 */
SelectBox &SelectBox::addItem(int index, std::string str)
{
  if(str.empty())
    return *this;
  items_map[index]=str;
  index2map.push_back(index);
  return *this;
}

/**
 * @brief Přesune kurzor o políčko nahoru
 */
void SelectBox::up(){   
  if(items_map.empty())
    return;
  menu_driver(my_menu, REQ_UP_ITEM); 
  wrefresh(my_menu_win);
}

/**
 * @brief Přesune kurzor o políčko dolů
 */
void SelectBox::down(){
  if(items_map.empty())
    return;
  menu_driver(my_menu, REQ_DOWN_ITEM);
  wrefresh(my_menu_win);
}

/**
 * @brief Aktuálně vybrané políčko
 * @return Vrací ID vybraného políčka, -1 při neúspěchu
 */
int SelectBox::selected(){   
  if(items_map.empty())
    return -1;
  else
    return index2map[my_menu->curitem->index];
}


/**
 * @brief Opuštění boxu - zešednutí
 */
void SelectBox::leave()
{
  wattron(my_menu_win,COLOR_PAIR(color_dark));
  drawBox();
  wattroff(my_menu_win,COLOR_PAIR(color_dark));
  wrefresh(my_menu_win);
}

/**
 * @brief Zvýranění boxu
 */
void SelectBox::enter()
{
  wattron(my_menu_win,COLOR_PAIR(color_active));
  drawBox();
  wattroff(my_menu_win,COLOR_PAIR(color_active));
  wrefresh(my_menu_win);
}



#endif // SELECTBOX_HPP
