#-------------------------------------------------
#
# Project created by QtCreator 2014-04-16T19:26:46
#
#-------------------------------------------------

QT       += core gui
QT       += network



include(../icp.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app



SOURCES += main.cpp\
        main_menu.cpp \
    new_game.cpp \
    game_window.cpp \
    join_game.cpp \
	game_over.cpp

HEADERS  += main_menu.h \
    new_game.h \
    game_window.h \
    join_game.h \
    ../shared/maze.hpp \
	game_over.h

FORMS    += main_menu.ui \
    new_game.ui \
    game_window.ui \
    join_game.ui \
	game_over.ui

LIBS += ../interface/libinterface.a    
LIBS += ../shared/libmaze.a
PRE_TARGETDEPS += ../shared/libmaze.a
PRE_TARGETDEPS += ../interface/libinterface.a




