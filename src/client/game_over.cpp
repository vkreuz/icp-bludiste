/**
 * @file game_over.cpp
 * @author Vladimír Čillo
 * @brief Zobrazenie výsledku hry
 *
 */



#include "game_over.h"
#include "ui_game_over.h"
#include "game_window.h"
#include "../interface/interface.hpp"



/**
 * @brief Automaticky generovaný konštruktor
 */
game_over::game_over(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::game_over)
{
    ui->setupUi(this);
}


/**
 * @brief Automaticky generovaný deštruktor
 */
game_over::~game_over()
{
    delete ui;
}



/**
 * @brief Metóda zobrazí okno a vykreslí do neho výhercu
 *
 * Počíta sa s možnosťou viacerých výhercov, maximálne 4.
 */
void game_over::show_results()
{
    int color, dir;
    int num_winners = Interface::getInstance()->winners.size() ;

    std::vector<int> id_vector(Interface::getInstance()->winners);

    switch(num_winners)
    {
        case 4:
                color = Interface::getInstance()->player_index(id_vector.back());
                id_vector.pop_back();
                dir = Interface::getInstance()->get_player(color)->dir;
                this->ui->winner4->setPixmap(QPixmap::fromImage(images_array[5*dir + color]));

        case 3:
                color = Interface::getInstance()->player_index(id_vector.back());
                id_vector.pop_back();
                dir = Interface::getInstance()->get_player(color)->dir;
                this->ui->winner3->setPixmap(QPixmap::fromImage(images_array[5*dir + color]));

        case 2:
                color = Interface::getInstance()->player_index(id_vector.back());
                id_vector.pop_back();
                dir = Interface::getInstance()->get_player(color)->dir;
                this->ui->winner2->setPixmap(QPixmap::fromImage(images_array[5*dir + color]));

        case 1:
                color = Interface::getInstance()->player_index(id_vector.back());
                id_vector.pop_back();
                dir = Interface::getInstance()->get_player(color)->dir;
                this->ui->winner1->setPixmap(QPixmap::fromImage(images_array[5*dir + color]));
                break;

        default: break;
    }


    this->setWindowFlags(Qt::WindowStaysOnTopHint);
    this->setModal(true);
    this->show();
}

