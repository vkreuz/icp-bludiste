/**
 * @file game_over.h
 * @author Automaticky generovaný súbor
 * @brief Automaticky generovaný súbor
*/

#ifndef GAME_OVER_H
#define GAME_OVER_H

#include <QDialog>

namespace Ui {
class game_over;
}

class game_over : public QDialog
{
    Q_OBJECT

public:
    explicit game_over(QWidget *parent = 0);
    ~game_over();
    void show_results();



private:
    Ui::game_over *ui;
};

#endif // GAME_OVER_H
