/**
 * @file game_window.cpp
 * @author Vladimír Čillo
 * @brief Implementácia metód pre obsluhu herného okna
 *
*/

#include "game_window.h"
#include "ui_game_window.h"
#include "../interface/interface.hpp"
#include "../shared/maze.hpp"


#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QScrollBar>
#include <QMessageBox>
#include <QtAlgorithms>

#define HLIDAC 4


#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3



QImage images_array[5*4];


/**
 * @brief Automaticky generovaný konštruktor, doplnený o:
 * - inicializáciu atribútu loaded_images
 * - zapnutie filtrovania udalostí v okne
 *
 * @param parent
 */
Game_window::Game_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Game_window)
{
    ui->setupUi(this);
    loaded_images = false;
    qApp->installEventFilter(this);
}


Game_window::~Game_window()
{
    delete ui;
}


/**
 * @brief Načíta obrázky hráčov a hlídačov do poľa images_array
 *
 * Po úspešnom vykonaní nastaví loaded_images na true, čím sa zabráni
 * opätovnému vykonaniu funkcie
 */
void Game_window::init_graphics()
{

    if( loaded_images )
        return;

    QMessageBox information;
    information.setText("Loading resources...");
    information.setWindowTitle("Loading");
    information.setEnabled(false);
    information.show();

    QImage image_0( "images/player_green_up.png" );
    if ( image_0.isNull() )
        throw("Unable to open player image 0");

    QImage image_1( "images/player_red_up.png" );
    if ( image_1.isNull() )
        throw("Unable to open player image 1");

    QImage image_2( "images/player_blue_up.png" );
    if ( image_2.isNull() )
        throw("Unable to open player image 2");

    QImage image_3( "images/player_yellow_up.png" );
    if ( image_3.isNull() )
        throw("Unable to open player image 3");

    QImage image_4( "images/hlidac_up.png" );
    if ( image_4.isNull() )
        throw("Unable to open player image 4");

    QImage image_5( "images/player_green_right.png" );
    if ( image_5.isNull() )
        throw("Unable to open player image 5");

    QImage image_6( "images/player_red_right.png" );
    if ( image_6.isNull() )
        throw("Unable to open player image 6");

    QImage image_7( "images/player_blue_right.png" );
    if ( image_7.isNull() )
        throw("Unable to open player image 7");

    QImage image_8( "images/player_yellow_right.png" );
    if ( image_8.isNull() )
        throw("Unable to open player image 8");

    QImage image_9( "images/hlidac_right.png" );
    if ( image_9.isNull() )
        throw("Unable to open player image 9");

    QImage image_10( "images/player_green_down.png" );
    if ( image_10.isNull() )
        throw("Unable to open player image 10");

    QImage image_11( "images/player_red_down.png" );
    if ( image_11.isNull() )
        throw("Unable to open player image 11");

    QImage image_12( "images/player_blue_down.png" );
    if ( image_12.isNull() )
        throw("Unable to open player image 12");

    QImage image_13( "images/player_yellow_down.png" );
    if ( image_13.isNull() )
        throw("Unable to open player image 13");

    QImage image_14( "images/hlidac_down.png" );
    if ( image_14.isNull() )
        throw("Unable to open player image 14");

    QImage image_15( "images/player_green_left.png" );
    if ( image_15.isNull() )
        throw("Unable to open player image 15");

    QImage image_16( "images/player_red_left.png" );
    if ( image_16.isNull() )
        throw("Unable to open player image 16");

    QImage image_17( "images/player_blue_left.png" );
    if ( image_17.isNull() )
        throw("Unable to open player image 17");

    QImage image_18( "images/player_yellow_left.png" );
    if ( image_18.isNull() )
        throw("Unable to open player image 18");

    QImage image_19( "images/hlidac_left.png" );
    if ( image_19.isNull() )
        throw("Unable to open player image 19");

    images_array[0]  = image_0;
    images_array[1]  = image_1;
    images_array[2]  = image_2;
    images_array[3]  = image_3;
    images_array[4]  = image_4;
    images_array[5]  = image_5;
    images_array[6]  = image_6;
    images_array[7]  = image_7;
    images_array[8]  = image_8;
    images_array[9]  = image_9;
    images_array[10] = image_10;
    images_array[11] = image_11;
    images_array[12] = image_12;
    images_array[13] = image_13;
    images_array[14] = image_14;
    images_array[15] = image_15;
    images_array[16] = image_16;
    images_array[17] = image_17;
    images_array[18] = image_18;
    images_array[19] = image_19;

    loaded_images = true;

    this->DrawMaze();
    this->setMouseTracking(true);
    this->setModal(true);
    this->setWindowTitle("Hra");
    this->setWindowFlags(Qt::Window);
    information.hide();
    this->show();
}




/**
 * @brief Filtruje udalosti, ktoré nastávajú v hernom okne.
 *
 * Metóda implementuje:
 * - posuv scény na základe pohybu myši
 * - ovládanie hry pomocou klávesnice
 *
 * @param obj Objekt, na ktorom udalosť nastala
 * @param event Informácie o udalosti, ktorá nastala (typ, súradnice, a pod.)
 * @return True ak bola udalosť spracovaná, False ak sa ma udalosť delegovať na potomka.
 *
 */
bool Game_window::eventFilter(QObject *obj, QEvent *event)
{

  if (event->type() == QEvent::MouseMove) // Pohyb mysou
  {

      if( obj->parent() == 0 )
          return false;

      /* odfiltrovanie objektov, ktorych MouseMove event nechceme spracovat
        t.j. ktorych mouseMoveEvent neposuva pohlad v hernom okne  */
      if( obj->objectName().toStdString().compare("Game_windowWindow") == 0)
          return false;

      else if( obj->objectName().toStdString().compare("qt_scrollarea_viewport") == 0 )
          return true;

      else if( obj->objectName().toStdString().compare("game_output") == 0)
          return true;

      else if( obj->objectName().toStdString().compare("playerInput") == 0)
          return true;

      else if( obj->objectName().toStdString().compare("label") == 0)
          return true;

      else if( obj->objectName().toStdString().compare("submitCommand_button") == 0)
          return true;


    /* Posuv scény za kurzorom myši */
    QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);

    int scene_width = this->ui->graphicsView->scene()->width();
    float x = mouseEvent->pos().x(); // X-ova suradnica bodu, kde nastala udalost
    float scroll_max_h = this->ui->graphicsView->horizontalScrollBar()->maximum();
    float ratio = scene_width / scroll_max_h;

    x /=  scroll_max_h ;
    x *= ratio;

    this->ui->graphicsView->horizontalScrollBar()->setValue( x * scroll_max_h   ); // posuv scroll-baru
  }


  /* Ak bolo stlacene tlacidlo a je nastaveny rezim ovladania pomocou klavesnice */
  else if(event->type() == QEvent::KeyPress && this->ui->keyboard_on->isChecked() )
  {
      if( Interface::getInstance()->local_player_killed )
      {
          if( Interface::getInstance()->game_runnig() ) // ak hra bezi, udalost je obsluzena
              return true;
          else
              return false;
      }

      switch ( static_cast<QKeyEvent*>(event)->key())
      {
          case Qt::Key_Left:    turnPlayer(LEFT);
                                break;

          case Qt::Key_Up:      turnPlayer(UP);
                                break;

          case Qt::Key_Right:   turnPlayer(RIGHT);
                                break;

          case Qt::Key_Down:    turnPlayer(DOWN);
                                break;

          case Qt::Key_W:       Interface::getInstance()->send("150\n"); // GO
                                break;

          case Qt::Key_S:       Interface::getInstance()->send("151\n"); // STOP
                                break;

          case Qt::Key_E:       Interface::getInstance()->send("155\n"); // OPEN
                                break;

          case Qt::Key_A:       Interface::getInstance()->send("154\n"); // TAKE
                                break;

          default:              break;
      }

      /* Ak hra nebezi, obsluha klaves je vypnuta */
      if(Interface::getInstance()->game_runnig())
         return true;
      else
         return false;
  }

  return false;
}



/**
 * @brief Otočí hráča zadaným smerom
 *
 * Ak je hráč daným smerom už otočený, nestane sa nič. Hráč sa otáča o 90° doprava, až dokým
 * nie je otočený požadovaným smerom.
 *
 * @param new_dir Nový smer, ktorým má byť hráč otočený.
 *
 */
void Game_window::turnPlayer( int new_dir )
{
    int current_dir = Interface::getInstance()->get_local_player()->dir;

    if( current_dir == new_dir )
        return;

    switch( new_dir )
    {
        case UP:
        {
            for( int i = current_dir; i != UP; i = (i+1)%4 )
                    Interface::getInstance()->send("153\n");
            break;
        }
        case RIGHT:
        {
            for( int i = current_dir; i != RIGHT; i = (i+1)%4 )
                    Interface::getInstance()->send("153\n");
            break;
        }
        case DOWN:
        {
            for( int i = current_dir; i!= DOWN; i = (i+1)%4 )
                    Interface::getInstance()->send("153\n");
            break;
        }

        case LEFT:
        {
            for( int i = current_dir; i!= LEFT; i = (i+1)%4 )
                    Interface::getInstance()->send("153\n");
            break;
        }

        default: throw("gameWindow: turnPlayer(): invalid dir detected");

    }

}








/**
 * @brief Pri stlačení klávesy ENTER v hernom okne sa vyvolá obsluha tlačidla Ok
 */
void Game_window::on_playerInput_returnPressed()
{
    this->on_submitCommand_button_clicked();
}




/**
 * @brief Odosiela príkaz zadaný uživateľom na server
 *
 * V prípade, že režim hry nie je nastavený na zadávanie príkazov, neurobí nič.
 * Po odoslaní príkazu sa textové pole, kde bol príkaz zadaný, vymaže.
 */
void Game_window::on_submitCommand_button_clicked()
{
    if( !this->ui->commands_on->isChecked() )
        return;

    Interface::getInstance()->command( ui->playerInput->text().toStdString()  );
    ui->playerInput->clear();
}




/**
 * @brief Vykreslí bludisko do herného okna
 *
 * Scéna preberá vlastníctvo všetkých svojích položiek - uvoľnovanie pamäti rieši metóda clear().
 */
void Game_window::DrawMaze() const
{
    QGraphicsScene* scene = this->ui->graphicsView->scene();
    if( scene == nullptr )
        scene = new QGraphicsScene;
    else
        scene->clear();

    int scene_width = Interface::getInstance()->Cols() * OBJECT_SIZE;
    int scene_height = Interface::getInstance()->Rows() * OBJECT_SIZE;

    /* nastavi rozmery sceny */
    scene->setSceneRect(0,0, scene_width, scene_height);

    /* skryje scroll-bary */
    this->ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    /* nastavi pozadie */
    this->ui->graphicsView->setStyleSheet("background-color: black;");
    this->ui->graphicsView->setScene(scene);

    const int rows = Interface::getInstance()->Rows();
    const int cols = Interface::getInstance()->Cols();

    QImage image_wall("images/wall.jpg");
    QImage image_floor("images/floor.png");
    QImage image_gate("images/locked_gate.png");
    QImage image_key("images/key.png");
    QImage image_goal("images/goal.png");


    std::vector<QGraphicsPixmapItem*> bludiste(rows*cols);

    Object::t_type actual;

    for(int i = 0; i < rows; i++)
    {
        for(int j =0; j< cols; j++)
        {
            actual = Interface::getInstance()->getMaze()->at(i*cols + j).get_type();
            switch(actual)
            {
                case Object::EMPTY:
                    bludiste[i*cols + j] = new QGraphicsPixmapItem(QPixmap::fromImage(image_floor));
                    break;

                case Object::GATE:
                    bludiste[i*cols + j] = new QGraphicsPixmapItem(QPixmap::fromImage(image_gate));
                    break;

                case Object::KEY:
                    bludiste[i*cols + j] = new QGraphicsPixmapItem(QPixmap::fromImage(image_key));
                    break;

                case Object::WALL:
                    bludiste[i*cols + j] = new QGraphicsPixmapItem(QPixmap::fromImage(image_wall));
                    break;

                case Object::GOAL:
                    bludiste[i*cols + j] = new QGraphicsPixmapItem(QPixmap::fromImage(image_goal));
                    break;

                default:
                    throw("DrawMaze: Error in maze");

            }

            bludiste[i*cols + j]->setPos( j*OBJECT_SIZE, i*OBJECT_SIZE );
            scene->addItem(bludiste[i*cols + j]);

        }
    }

}





/**
 * @brief Metóda prída hráča do scény - do herného okna
 * @param x X-ová súradnica hráča
 * @param y Y-ová súradnica hráča
 * @param direction Smer, ktorým je hráč otočený
 * @param color Farba hráča
 * @param tooltip Text, ktorý sa zobrazí po ukázaní myšou na hráča
 */
void Game_window::addPlayerToScene(int x, int y, int direction, int color, std::string tooltip )
{
    QGraphicsPixmapItem* i_player = new QGraphicsPixmapItem(QPixmap::fromImage(images_array[5* direction + color]));
    i_player->setPos(x*OBJECT_SIZE,y*OBJECT_SIZE); 
    i_player->setToolTip( tooltip.c_str() );
    this->ui->graphicsView->scene()->addItem(i_player);
}






/**
 * @brief Vykreslí hráčov a hlídačov do herného okna
 *
 * Ak sú u hráča dostupné informácie o čase strávenom v hre a pod., tak sa nastavia danému hráčovi
 * ako tooltip - text, ktorý sa zobrazí pri ukázaní myšou na hráča.
 *
 */
void Game_window::DrawPlayers( )
{
    player_info* curr_player;
    int minutes, seconds;
    std::vector<post_game_info> players_info_data(Interface::getInstance()->players_data);

    /* Vykreslenie hráčov */
    for(int i = 0; i < MAX_PLAYERS; i++)
    {
        curr_player = Interface::getInstance()->get_player(i);
        if( curr_player->id == -1 ) // hrac je mrtvy alebo neexistuje
            continue;

        std::string tooltip("Player " + std::to_string(i+1) );

        if( players_info_data.size() != 0) // ak su dostupne informacie o hracoch
        {

            for ( auto it = std::begin(players_info_data); it != std::end(players_info_data); ++it)
            {
                if( it->ID == curr_player->id )
                {
                    minutes = it->player_time / 60;
                    seconds = it->player_time % 60;
                    tooltip += "\n Čas strávený ve hře: " + std::to_string(minutes) + " minút " + std::to_string(seconds) + " sekúnd" ;
                    minutes = it->game_time / 60;
                    seconds = it->game_time % 60;
                    tooltip += "\n Celkový čas hry: " + std::to_string(minutes) + " minút " + std::to_string(seconds) + " sekúnd";
                    tooltip += "\n Počet prejdených políčok: " + std::to_string(it->num_steps);

                }
            }
        }

        this->addPlayerToScene( curr_player->x, curr_player->y, curr_player->dir, i, tooltip ); // i - poradie hráča v poli hráčov - určuje farbu
    }


    /* Vykreslenie hlídačov */
    std::vector<player_info> hlidaci(*Interface::getInstance()->getGuards());

    for (auto it = std::begin (hlidaci); it != std::end (hlidaci); ++it)
    {
        this->addPlayerToScene(it->x,it->y,it->dir,HLIDAC, "Hlidac" );
    }
}








/**
 * @brief Metóda pre zobrazenie spätnej väzby
 *
 * Spätná väzba sa týka buď úspešnosti/neúspešnosti vykonania príkazu
 * alebo
 * zabitia niektorého z hráčov
 */
void Game_window::Output()
{
    using namespace std;

    string data = Interface::getInstance()->response;

    if( data.empty() )
        return;

    string current_text = this->ui->game_output->toPlainText().toStdString();

    data += "\n";
    data += current_text;
    QString new_data(data.c_str());

    this->ui->game_output->setText(new_data);


    /* udaje su vypisane, mozu sa vymazat */
    Interface::getInstance()->response.clear();

}





/**
 * @brief Metóda volaná pri zatvorení herného okna
 *
 * Ak hra už skončila, okno sa zatvorí. V opačnom prípade sa uživateľovi zobrazí výzva s upozornením,
 * že hra ešte neskončila a vyžaduje sa potvrdenie ukončenia hry. V prípade potvrdenia sa na server odošle
 * informácia o tom, že hráč opustil hru.
 *
 * @param event Udalosť zatvorenia okna
 *
 */
void Game_window::closeEvent ( QCloseEvent * event )
{
    if(Interface::getInstance()->game_runnig())
    {
        event->ignore();
        if (QMessageBox::Yes == QMessageBox::question(this, "Potvrdit ukončení", "Hra ješte neskončila", QMessageBox::Yes|QMessageBox::No))
        {
            Interface::getInstance()->send("199 \n");
            QMessageBox information;
            information.setText("Disconnecting...");
            information.setWindowTitle("Quit");
            information.setEnabled(false);
            information.show();
            this->ui->graphicsView->scene()->clear();
            information.hide();
            event->accept();
        }
    }
    else
        event->accept();

}





