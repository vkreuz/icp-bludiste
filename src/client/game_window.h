/**
 * @file game_window.h
 * @author Vladimír Čillo
 * @brief Game_window - trieda herného okna
 *
 *
*/


#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <QDialog>
#include "../interface/interface.hpp"
#include <vector>
#include <QGraphicsView>


/**
 * Veľkosť textúry v pixeloch
 */
#define OBJECT_SIZE 64


/**
 * Pole pre uchovávanie obrazkov použitých v hre
 * 5 = 4 obrázky hráčov + 1 obrázok hlídač
 * 4 smery, spolu 5*4 obrázkov
 *
 * Pole sa zinicializuje vo funkcii init_graphics().
 *
 * Farbu a otocenie hraca na prislusny obrazok v images_array mapuje funkcia 5*dir + color
 *
 */
extern QImage images_array[5*4];



namespace Ui {
class Game_window;
}


/**
 * @brief Trieda herného okna
 *
 * Poskytuje metódy pre prácu s herným oknom - zobrazovanie hráčov, obsluha myši a pod.
 */
class Game_window : public QDialog
{
    Q_OBJECT
    bool loaded_images; ///< indikátor načítania obrázkov do poľa images_array

public:
    Ui::Game_window *ui;

    /* Metódy implementované v súbore gamewindow.cpp */

    bool eventFilter(QObject *obj, QEvent *event);
    bool sceneEventFilter( QGraphicsItem *pWatched, QEvent* pEvent);
    void DrawMaze() const;
    void DrawPlayers( );
    void addPlayerToScene(int x, int y, int direction, int color, std::string tooltip );
    void init_graphics();
    void turnPlayer( int dir );
    void Output();
    void closeEvent ( QCloseEvent * event );
    explicit Game_window(QWidget *parent = 0); //< konstruktor
    ~Game_window();


private slots:
    void on_playerInput_returnPressed();
    void on_submitCommand_button_clicked();

};


#endif // GAME_WINDOW_H
