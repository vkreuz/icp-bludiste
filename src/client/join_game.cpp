/**
 * @file join_game.cpp
 * @author Vladimír Čillo
 * @brief Implementácia metód pre obsluhu obrazovky na pripojenie do hry
 *
*/

#include "join_game.h"
#include "ui_join_game.h"
#include "game_window.h"
#include "game_over.h"

#include "../interface/interface.hpp"

#include <QDebug>
#include <QMessageBox>


#include <string>
#include <sstream>



join_game::join_game(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::join_game)
{
    ui->setupUi(this);
}



join_game::~join_game()
{
    delete ui;
}


/**
 * @brief Po kliknutí na tlačidlo "Zpět" vráti uživateľa späť do hlavného menu
 */
void join_game::on_back_button_clicked()
{
    connect(this, SIGNAL(returning()), parentWidget(), SLOT(onBack_button_clicked()));
    Interface::getInstance()->games(false);
    Interface::getInstance()->init(false);
    emit returning();
}


/**
 * @brief Ak sú zadané všetky potrebné údaje, pripojí klienta k hre.
 *
 * Metóda cyklicky získava nový stav hry a vykresľuje ho do herného okna.
 * Priebežne sa na hracej obrazovke zobrazujú informácie o úspešnosti
 * príkazov a zabitiach hráčov.
 */
void join_game::on_join_game_button_clicked()
{
    std::string game = ui->list_of_available_games->currentText().toStdString();
    try {

        if( game.empty() )
            throw("Vyberte hru ");

        Maze bludiste(Interface::getInstance()->downloadMaze(0));
        Interface::getInstance()->setMap(bludiste);

        std::string game_name(ui->list_of_available_games->currentText().toStdString().c_str());
        int game_id = Interface::getInstance()->name2id( game_name.c_str()  , true); // id hry na ktoru sa hrac chce pripojit


        /* SPUSTENIE HRY */
        Interface::getInstance()->game_init(false, game_id );

        Game_window* game_window = new Game_window; // vytvori herne okno
        game_window->setAttribute(Qt::WA_DeleteOnClose, true); // po zatvoreni okna sa uvolni pamat
        game_window->init_graphics();

        while( Interface::getInstance()->game()  )
        {
            game_window->DrawMaze();
            game_window->DrawPlayers();
            game_window->Output();
        }

        if( game_window->isVisible() )
            game_window->DrawPlayers(); // zobrazi stav po skonceni hry

        game_over* game_over_window = new game_over(0);
        game_over_window->setAttribute(Qt::WA_DeleteOnClose, true);
        game_over_window->show_results();
    }
    catch ( const char *exc) {
        QMessageBox m;
        m.setText(exc);
        m.setWindowTitle("Chyba");
        m.exec();
        return;
    }
}


/**
 * @brief Obsluha tlačidla pre načítanie rozohraných hier
 *
 * Prostredníctvom triedy Interface získa rozohraté hry a pridá ich
 * do zoznamu hier, z ktorého uživateľ môže vybrať hru, ku ktorej sa chce pripojiť.
 */
void join_game::on_load_games_button_clicked()
{
    using namespace std;

    string host(ui->server_field->text().toStdString());
    int port = ui->port->text().toInt();

    try {
        if ( !Interface::getInstance()->games() ) // jestli dostupne hry jeste nejsou nacteny
        {
            if( !Interface::getInstance()->init() )
            {
                if( host.empty())
                    throw("Pole server nesmí být prázdne ");

                else if( port == 0 )
                    throw("Zadejte port ");

                Interface::getInstance()->init(host, port);
            }

            istringstream f(Interface::getInstance()->getGames());
            string line;


            /* pridavanie dostupnych hier do list boxu */
            while (std::getline(f, line))
            {
                if( line.empty() )
                    continue;

                ui->list_of_available_games->addItem(line.c_str());
            }
        }
    }
    catch ( const char *exc) {
        QMessageBox m;
        m.setText(exc);
        m.setWindowTitle("Chyba");
        m.exec();
        return;
    }
}
