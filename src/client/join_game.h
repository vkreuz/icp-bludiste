/**
 * @file join_game.h
 * @author Automaticky generovaný súbor
 * @brief Automaticky generovaný súbor
*/


#ifndef JOIN_GAME_H
#define JOIN_GAME_H

#include <QDialog>



namespace Ui {
class join_game;
}

class join_game : public QDialog
{
    Q_OBJECT

public:
    explicit join_game(QWidget *parent = 0);
    ~join_game();

signals:
    void returning();


private slots:
    void on_back_button_clicked();
    void on_join_game_button_clicked();

    void on_load_games_button_clicked();

private:
    Ui::join_game *ui;
};

#endif // JOIN_GAME_H
