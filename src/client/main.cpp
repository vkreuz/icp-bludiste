/**
 * @file client/main.cpp
 * @author Automaticky generovaný súbor
 * @brief Funkcia main pre grafického klienta
 *
*/


#include "main_menu.h"
#include <QApplication>


int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    Main_Menu m_window;
    m_window.show();
    return a.exec();
}
