/**
 * @file main_menu.cpp
 * @author Vladimír Čillo
 * @brief Implementácia obsluhy tlačidiel v hlavnom menu
 *
*/


#include "main_menu.h"
#include "ui_main_menu.h"
#include <QMessageBox>
#include <QDebug>
#include "new_game.h"
#include "join_game.h"



Main_Menu::Main_Menu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Main_Menu)
{
    ui->setupUi(this);
}



Main_Menu::~Main_Menu()
{
    delete ui;
}



/**
 * @brief Po kliknutí na tlačidlo "Pripojit ke hře" prepne uživateľa do okna s možnosťami pre pripojenie k hre
 */
void Main_Menu::on_join_game_button_clicked()
{
    join_game* join_game_menu = new join_game;
    this->main_window = this->centralWidget();
    this->main_window->setParent(0);
    this->setCentralWidget(  join_game_menu );
}


/**
 * @brief Po kliknutí na tlačidlo "Nová hra" prepne uživateľa do okna s možnosťami
 *        pre vytvorenie novej hry
 */
void Main_Menu::on_new_game_button_clicked()
{
    new_game* new_game_menu = new new_game;
    this->main_window = this->centralWidget();
    this->main_window->setParent(0);
    this->setCentralWidget(  new_game_menu );
}



/**
 * @brief Po kliknutí na tlačidlo "Zpět" vráti uživateľa späť do hlavného menu
 */
void Main_Menu::onBack_button_clicked()
{
    this->setCentralWidget(  this->main_window );
}




/**
 * @brief Zobrazí mini-okno s informáciami o autoroch.
 */
void Main_Menu::on_about_button_clicked()
{
    QMessageBox msgBox;
    msgBox.setText(" © 2014 Vladimír Čillo & Vítězslav Kříž ");
    msgBox.setWindowTitle("C++ projekt 2014");
    msgBox.exec();
}


/**
 * @brief Ukončí program
 */
void Main_Menu::on_quit_button_clicked()
{
    this->close();
    qApp->quit();
}




/**
 * @brief Zobrazí okno s popisom ovládania hry
 */
void Main_Menu::on_controls_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("C++ projekt 2014");
    msgBox.setText("\n\n Ovládaní hry\n\n Pomocí klávesnice: \n \
        Šípka doleva otočí hráče doleva (pokud již je otočen doleva, neprovede nic)\n \
        Šípka doprava otočí hráče doprava \n \
        Šípka nahoru otočí hráče nahoru \n \
        Šípka dolu otočí hráče dolu \n \
        E\t hráč otevře bránu klíčem (pokud stojí před bránou a má klíč) \n \
        A\t hráč vezme klíč (pokud je na políčku před ním) \n \
        S\t zastaví pohyb hráče (pokud již stojí, neprovede nic) \n \
        W\t hráč jde rovně dokud nenarazí na jiný objekt (brána, zeď, jiný hráč) \n\n Pomocí příkazů: \n \
        go - hráč jde rovně dokud nenarazí na jiný objekt (brána, zeď, jiný hráč) \n \
        stop - zastaví pohyb hráče (pokud již stojí, neprovede nic) \n \
        left - hráč se otočí doleva o 90° \n \
        right - hráč se otočí doprava o 90°\n \
        take - hráč vezme klíč (pokud je na políčku před ním) \n \
        open - hráč otevře bránu klíčem (pokud stojí před bránou a má klíč)\n\n \
        Kolečko myši - posuv nahoru a dolů \n \
        Pohyb myši - posuv doleva a doprava \n ");
    msgBox.exec();
}









