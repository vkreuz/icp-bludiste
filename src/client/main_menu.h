/**
 * @file main_menu.h
 * @author Automaticky generovaný súbor
 * @brief Automaticky generovaný súbor
*/

#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <QMainWindow>
#include <QStackedWidget>




namespace Ui {
class Main_Menu;
}

class Main_Menu : public QMainWindow
{
    Q_OBJECT

public:
    explicit Main_Menu(QWidget *parent = 0);
    ~Main_Menu();

private slots:
    void on_quit_button_clicked();
    void on_about_button_clicked();
    void on_join_game_button_clicked();
    void on_new_game_button_clicked();


    void onBack_button_clicked();
    void on_controls_clicked();

private:
    QWidget *main_window;
    Ui::Main_Menu *ui;
};




#endif // MAIN_MENU_H
