/**
 * @file new_game.cpp
 * @author Vladimír Čillo
 * @brief Implementácia metód pre obsluhu obrazovky na vytvorenie novej hry
 *
*/


#include "new_game.h"
#include "ui_new_game.h"
#include "game_window.h"
#include "game_over.h"

#include <QDebug>
#include <QMessageBox>
#include <QGraphicsPixmapItem>


#include <sstream>
#include <string>

#include "../interface/interface.hpp"



new_game::new_game(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::new_game)
{
    ui->setupUi(this);
}



new_game::~new_game()
{
    delete ui;
}



/**
 * @brief Po kliknutí na tlačidlo "Zpět" vráti uživateľa späť do hlavného menu
 */
void new_game::on_new_game_back_button_clicked()
{
    connect(this, SIGNAL(returning()), parentWidget(), SLOT(onBack_button_clicked()));
    Interface::getInstance()->maps(false);
    Interface::getInstance()->init(false);
    emit returning();
}


/**
 * @brief Ak sú zadané všetky potrebné údaje, pošle na server
 * žiadosť o vytvorenie novej hry.
 *
 * Metóda cyklicky získava nový stav hry a vykresľuje ho do herného okna.
 * Priebežne sa na hracej obrazovke zobrazujú informácie o úspešnosti
 * príkazov a zabitiach hráčov.
 *
 */
void new_game::on_new_game_submit_button_clicked()
{
    std::string map_name;
    std::string game_name;
    int map_id;

    try {

        float delay = ui->delayField->text().toFloat(); // prodleva
        if( delay == 0 )
            throw("Zadejte prodlevu ");

        if( delay < 0.5 || delay > 5.0 )
            throw("Prodleva musí být v intervalu <0.5,5> ");

        map_name = ui->comboBox_games->currentText().toStdString(); // nazov mapy
        game_name = ui->game_name->text().toStdString();

        if( map_name.empty() )
            throw("Zvolte mapu ");

        else if ( game_name.empty() )
            throw("Zadejte jméno hry");

        else
            map_id = Interface::getInstance()->name2id( map_name.c_str()  , false); // id mapy, ktora sa stiahne zo servra


        /* SPUSTENIE HRY */

        Interface::getInstance()->game_init(true, map_id, game_name.c_str(), delay );
        Game_window* game = new Game_window; // vytvori herne okno
        game->init_graphics();

        while( Interface::getInstance()->game()  )
        {
            game->DrawMaze();
            game->DrawPlayers();
            game->Output();
        }

        if( game->isVisible() )
            game->DrawPlayers(); // zobrazi stav po skonceni hry

        game_over* game_over_window = new game_over(0);
        game_over_window->setAttribute(Qt::WA_DeleteOnClose, true);
        game_over_window->show_results();
    }
    catch ( const char *exc) {
        QMessageBox m;
        m.setText(exc);
        m.setWindowTitle("Chyba");
        m.exec();
        return;
    }

}




/**
 * @brief Obsluha tlačidla "Načíst dostupné mapy"
 *
 * Ak sú zadané všetky potrebné údaje, metóda pošle na server
 * žiadosť o dostupné mapy  a pridá ich do zoznamu
 * dostupných máp, z ktorého uživateľ môže zvoliť mapu, ktorú chce hrať.
 */
void new_game::on_checkServer_clicked()
{
    std::string host(ui->server_field->text().toStdString());
    int port = ui->port->text().toInt();


    try {
        if ( ! Interface::getInstance()->maps() ) // ak server este neposlal zoznam map
        {
            if( !Interface::getInstance()->init() )
            {
                if( host.empty() )
                    throw("Pole server nesmí být prázdne ");

                else if( port == 0 )
                    throw("Zadejte port ");

                Interface::getInstance()->init( host, port );
            }

            std::istringstream f(Interface::getInstance()->getMaps());
            std::string line;

            /* pridavani dostupnych map do list boxu v nabidce nove hry*/
            while (std::getline(f, line))
            {
                if( ! line.empty() )
                    ui->comboBox_games->addItem(line.c_str());
            }

            Interface::getInstance()->maps(true);
            Interface::getInstance()->init(true);
        }

    }
    catch ( const char *exc) {
        QMessageBox m;
        m.setText(exc);
        m.setWindowTitle("Chyba");
        m.exec();
        return;
    }
}


/**
 * @brief Pri vybraní konkrétnej mapy zo zonamu dostupných máp zobrazí
 *        náhľad danej mapy
 */
void new_game::on_comboBox_games_currentTextChanged()
{

    static QGraphicsScene tmp_scene;

    QGraphicsScene* scene = &tmp_scene;
    scene->clear();

    if(ui->comboBox_games->count() == 0)
        return;

    this->ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->ui->graphicsView->setStyleSheet("background-color: black;");
    this->ui->graphicsView->setScene(scene);

    std::string map_name = ui->comboBox_games->currentText().toStdString(); // nazov zvolenej mapy

    if( map_name.empty() )
        throw("Zvolte mapu ");
    else
    {
        try{
            int map_id = Interface::getInstance()->name2id( map_name.c_str()  , false); // id mapy, ktora sa stiahne zo servra
            Maze bludiste(Interface::getInstance()->downloadMaze(map_id));
            Interface::getInstance()->setMap(bludiste);
            ;
        }
        catch (const char* exc){
            QMessageBox m;
            m.setText(exc);
            m.setWindowTitle("Chyba");
            m.exec();
            qDebug() << exc;
            return;
        }
    }

    const int rows = Interface::getInstance()->Rows();
    const int cols = Interface::getInstance()->Cols();

    QImage small( "images/small.jpg");
    if( small.isNull() )
    {
        QMessageBox::information(this,"small.jpg","Error loading image");
        return;
    }

    int item_size = small.size().height();
    std::vector<QGraphicsPixmapItem*> bludiste(rows * cols);

    Object::t_type actual;

    /* vykreslenie nahladu */
    for(int i = 0; i < rows; i++)
    {
        for(int j =0; j < cols; j++)
        {
            actual = Interface::getInstance()->getMaze()->at(i*cols + j).get_type();
            switch(actual)
            {
                case Object::EMPTY:
                    bludiste[i*cols + j] = new QGraphicsPixmapItem(QPixmap::fromImage(small));
                    bludiste[i*cols + j]->setPos( j*item_size, i*item_size );
                    scene->addItem(bludiste[i*cols + j]);
                    break;

                default:
                    break;
            }
        }
    }

}



