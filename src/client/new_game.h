/**
 * @file new_game.h
 * @author Automaticky generovaný súbor
 * @brief Automaticky generovaný súbor
*/

#ifndef NEW_GAME_H
#define NEW_GAME_H

#include <QDialog>

namespace Ui {
class new_game;
}

class new_game : public QDialog
{
    Q_OBJECT

public:
    explicit new_game(QWidget *parent = 0);
    ~new_game();

signals:
    void returning();

private slots:
    void on_new_game_back_button_clicked();
    void on_new_game_submit_button_clicked(); 
    void on_checkServer_clicked();

    void on_comboBox_games_currentTextChanged();

private:
    Ui::new_game *ui;
};

#endif // NEW_GAME_H
