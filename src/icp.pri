
QMAKE_CXXFLAGS += -std=c++11  -Wall -Wextra -pedantic -g
#for merlin
QMAKE_CXX = g++-4.8
QMAKE_LINK_SHLIB = g++-4.8
QMAKE_LINK = g++-4.8
LIBS += -static-libstdc++


DEFINES+= QT_NO_DEBUG_OUTPUT
QMAKE_LFLAGS += -g
