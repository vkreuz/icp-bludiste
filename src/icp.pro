#----------------------------------------
# ICP Bludiste 2014
#----------------------------------------
# Autori:
#         Vitezslav Kriz
#         Vladimir Cillo
#########################################

TEMPLATE = subdirs

CONFIG += ordered



SUBDIRS = shared \
          interface \ 
          server \
          client \
          client-cli \
    

server.depends = shared
client.depends = shared interface
