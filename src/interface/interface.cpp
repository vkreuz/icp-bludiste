/** @file interface.cpp
* @author Vladimír Čillo
* @brief Implementácia klientskej časti rozhrania medzi klientom a servrom
*
* Modul implementuje metódy triedy Interface
*/



#include "interface.hpp"
#include <QEventLoop>
#include <sstream>



#define PLAYER_KILLED 1
#define PLAYER_DISCONECTED 2
#define BUFFER_SIZE 2600 // 50*50 + 100
#define BUFF_SIZE 400


/* kody odpovedi serveru */
#define NOT_UNDERSTOOD 599
#define GAME_UPDATE 550
#define INFORMATION 700
#define POST_GAME_INFO 600
#define GAME_OVER 800
#define DISCONNECT 501

/* USPESNOST OPERACII */
#define GO 0
#define STOP 1
#define LEFT 2
#define RIGHT 3
#define TAKE 4
#define OPEN 5





Interface* Interface::interf = nullptr;


/**
 * @brief Konštruktor ktorý nastaví Interface do počiatočného stavu
 * @param parent Odkaz na rodiča objektu.
 */
Interface::Interface(QObject *parent) :
    QObject(parent)
{
    _maps  = false;
    _init  = false;
    _games = false;
    game_is_runnig = false;
    map = nullptr;
    socket = nullptr;
    local_player_id = -1;
    local_player_killed = false;
    in_game_players = 0;

    for(int i = 0; i < MAX_PLAYERS; i++)
        players[i] = { -1, -1, -1, -1 };

}


/**
 * @brief Metóda zabezpečuje, aby bol pred spustením hry objekt v konzistentom stave
 *
 * Nastaví všetky premenné týkajúce sa hry na inicializačné hodnoty.
 */
void Interface::clean_up()
{
   game_is_runnig = false;
   local_player_killed = false;
   local_player_id = -1;
   in_game_players = 0;

   for(int i = 0; i < MAX_PLAYERS; i++)
       players[i] = { -1, -1, -1, -1 };

   guards.clear();
   response.clear();
   players_data.clear();
   winners.clear();

}




/**
 * @brief Handshake so serverom
 *
 * Skontroluje, či je odpoveďový kód v súlade s komunikačným protokolom.
*/
void Interface::readResponse()
{
    QEventLoop loop;
    int code;
    QObject::connect(socket, SIGNAL(readyRead()), &loop, SLOT(quit()));

    loop.exec();
    std::string msg = socket->readAll().constData();

    std::istringstream response(msg.c_str());
    response >> code;

    if( code != 500)
        throw("Nekompatibilny server !");

    qDebug() << "readResponse():" << msg.c_str();

}




/**
 * @brief Metóda na vytvorenie spojenia so serverom
 * @param host Hostname
 * @param port Číslo portu, na ktorom beží server
*/
void Interface::init( std::string host, int port )
{
    socket = new QTcpSocket(this);

    qDebug() << "connecting...";

    socket->connectToHost(host.c_str(), port );

    if(!socket->waitForConnected(10000)) // timeout
    {
        qDebug() << "Error: " << socket->errorString();
        throw( "Servr neodpovída." );
    }

    socket->write("100 \n");
    this->readResponse(); // precita a zahodi uvodnu spravu :-)
    this->init(true);
}





/**
 * @brief Vytvorí novú hru alebo pripojí klienta k už existujúcej hre
 * @param new_game Prepínač medzi novou hrou a pripojením k hre
 * @param id Identifikátor hry ku ktorej sa klient pripája alebo identifikátor mapy, na ktorej zakladá hru
 * @param name Meno hry, ktorú klient zakladá
 * @param delay Prodleva (v rozsahu 0.5 - 5)
 */
void Interface::game_init(bool new_game,  int id, const char* name , double delay)
{
    using namespace std;
    char data[BUFFER_SIZE];

    string str;
    ostringstream message;
    QEventLoop loop;
    int bytes_read;
    
    //vymazani starych udaju
    clean_up();

    QObject::connect(  socket, SIGNAL(readyRead()), &loop, SLOT(quit())  );

    if(new_game)
    {
        message << "120" << " "
                <<  id   << " "
                << delay << " "
                << MAX_PLAYERS << " "
                << name << "\n";
    }
    else
    {
        message << "130" << " "
                << id    << "\n" ;
    }

    socket->write(message.str().c_str());
    loop.exec();  // cakaj kym server neodpovie


    bytes_read = socket->readLine( data, BUFFER_SIZE );
    if( bytes_read == -1 )
        throw("game_init: readLine()");

    str = string( data );

    if( data[bytes_read-1] != '\n' ) // mapa neprisla cela
    {
        loop.exec(); // cakanie na zvysok mapy
        socket->readLine( data,BUFFER_SIZE );
        str += string(data);

    }


    istringstream response( str.c_str() );

    Maze tmp( this->parse_530(response, str) );
    game_is_runnig = true;

    this->setMap( tmp ); // nastavi sa mapa ktora pride ako odpoved od servra
    return;
}



/**
 * @brief Metóda konvertuje ID hráča na jeho index v poli hráčov
 * @param[in] player_id ID hráča
 * @return Index hráča v poli hráčov @see Interface::players
 */
int Interface::player_index( int player_id ) const
{
    for(int i = 0; i < MAX_PLAYERS; i++)
    {
        if( players[i].id == player_id )
            return i;
    }
    return -1;
}




/**
 * @brief Metóda konvertuje ID hlídača na jeho index vo vektore hlídačov
 * @param[in] guard_id ID Hlídača
 * @return Index vo vektore hlídačov @see Interface::guards
 */
int Interface::guard_index( int guard_id ) const
{
    int i = 0;
    for (auto it = begin (guards); it != end (guards); ++it, ++i) {
        if(it->id == guard_id)
            return i;
    }
    return -1;

}



/**
 * @brief Funkcia spracováva herné udalosti a na základe správ od serveru mení stav objektu Interface.
 *
 * Stavom sa rozumie : Pozície, smery a stavy hráčov
 *                     Pozície a smery hlídačov
 *                     Úspešnosť vykonania príkazov
 *                     Informácie o hráčovi potrebné po skončení hry a pod.
 *
 * @see Interface::guards
 * @see player_info
 * @see Interface::players
 * @see Interface::response
 * @return true Ak hra neskončila, inak false
 */
bool Interface::game( )
{

    qDebug() << "----------------------";
    qDebug() << "game():";

    using namespace std;

    char incoming_buffer[BUFF_SIZE];
    int response_code, op_code, result;
    char who;
    string tmp;

    QEventLoop loop;
    QObject::connect(  socket, SIGNAL(readyRead()), &loop, SLOT(quit())  );
    loop.exec();


    qDebug() << "Game tick, local player id:" << local_player_id;
    while( socket->canReadLine() ) // cyklus cez vsetky odpovedove kody, ktore prisli od servru
    {
        socket->readLine( incoming_buffer, BUFF_SIZE);
        tmp = incoming_buffer;
        qDebug() << tmp.c_str();
        istringstream response(tmp.c_str());
        response >> response_code;



        if( response_code == GAME_UPDATE )
        {
            int current_player_id, current_player_R, current_player_C, current_player_dir, current_player_state ;
            int hlidac_id, hlidac_R, hlidac_C, hlidac_dir;
            int player_ind;

            while( response >> who ) // cyklus cez vsetky zmeny v ramci kodu 550
            {
                if( who == 'P' ) // zmena stavu hraca
                {
                    response >> current_player_id; // player id
                    response >> current_player_R;
                    response >> current_player_C;
                    response >> current_player_dir;
                    response >> current_player_state;

                    if( current_player_state == PLAYER_KILLED )
                    {
                        player_ind = this->player_index(current_player_id);
                        if( player_ind == -1 ) // hrac zabity na spawne
                            continue;

                        players[player_ind] = { -1, -1, -1, -1}; // dany index je od teraz volny
                        in_game_players--;

                        if( current_player_id == local_player_id )
                        {
                            local_player_killed = true;
                            qDebug() << "*** You were killed ***";
                        }
                        
                        if(!this->response.empty()){
                          this->response += "\n";
                        }

                        this->response += "Player " + to_string(current_player_id) + " killed.";
                    }
                    else if( current_player_state == PLAYER_DISCONECTED )
                    {
                        player_ind = this->player_index(current_player_id);
                        if( player_ind == -1 )
                            continue;

                        players[player_ind] = { -1, -1, -1, -1};
                        in_game_players--;
                    }
                    else // player status: PLAYING
                    {
                        player_ind = this->player_index(current_player_id);

                        if( player_ind == -1 ) // dany hrac v hre este nie je
                        {
                            if( in_game_players < MAX_PLAYERS ) // ak hra nie je plna, je to novy hrac
                            {
                                /* RESPAWN */
                                if(current_player_id == local_player_id)
                                {
                                    local_player_killed = false;
                                    qDebug() << "*** Respawn ***";
                                }

                                /* Cyklus umiestni hraca na prvy volny index v poli hracov */
                                for(int i = 0; i < MAX_PLAYERS; i++)
                                {
                                    if(players[i].id == -1 )
                                    {
                                       players[i] = { current_player_id, current_player_C, current_player_R, current_player_dir };
                                       in_game_players++;
                                       break;
                                    }
                                }
                            }
                            else
                            {
                                qDebug() << current_player_id;
                                throw("game(): No such player");
                            }
                        }
                        else // zmena stavu hraca, ktory uz je v hre
                        {
                            players[player_ind] = { current_player_id, current_player_C, current_player_R, current_player_dir };
                        }
                    }
                }
                else if( who == 'H')
                {
                    player_info hlidac;

                    qDebug() << "Hlidac";

                    response >> hlidac_id; // id hlidaca
                    qDebug() << "guard_id:" << hlidac_id;

                    response >> hlidac_R;
                    qDebug() << "ROW:" << hlidac_R;

                    response >> hlidac_C;
                    qDebug() << "COL:" << hlidac_C;

                    response >> hlidac_dir;
                    qDebug() << "DIR:" << hlidac_dir;

                    hlidac = { hlidac_id, hlidac_C, hlidac_R, hlidac_dir };


                    int hlidac_index = this->guard_index(hlidac_id);

                    if( hlidac_index == -1 ) // novy hlidac
                        guards.push_back(hlidac);

                    else // zmena stavu hlidaca, ktory uz je v hre
                        guards[hlidac_index] = hlidac;

                }
                else if( who == 'K') // zdvihnuty/vlozeny kluc
                {
                    int key_row,key_column, what,MAX_COLUMNS;

                    response >> key_row;
                    response >> key_column;
                    response >> what;

                    MAX_COLUMNS = Cols();

                    if(what == 1)
                        getMaze()->at(key_row*MAX_COLUMNS + key_column) = Object(Object::KEY);
                    else
                        getMaze()->at(key_row*MAX_COLUMNS + key_column) = Object(Object::EMPTY);

                }
                else if( who == 'G' ) // otvorenie brany
                {
                    int gate_row,gate_column, MAX_COLUMNS;

                    response >> gate_row;
                    response >> gate_column;

                    MAX_COLUMNS = Cols();
                    getMaze()->at(gate_row*MAX_COLUMNS + gate_column) = Object(Object::EMPTY);

                }
                else
                {
                    qDebug() << "What is" << who;
                    break;
                }
            }
        }
        else if( response_code == INFORMATION )
        {
            /* sprava o uspesnosti prikazu */

            string log("Operace ");

            response >> op_code;
            switch(op_code)
            {
                case GO:
                    log += "GO";
                    break;

                case STOP:
                    log += "STOP";
                    break;

                case LEFT:
                    log += "LEFT";
                    break;

                case RIGHT:
                    log += "RIGHT";
                    break;

                case TAKE:
                    log += "TAKE";
                    break;

                case OPEN:
                    log += "OPEN";
                    break;

                default: qDebug() << "game(): op_code not understood:" << op_code;
                         break;
            }

            response >> result;
            if( result == 0 )
                log += ": OK";
            else
                log += ": FAIL";

            if(!this->response.empty()){
              this->response += "\n";
            }
            this->response += log;

        }
        else if( response_code == POST_GAME_INFO )
        {
            /* Logovanie informacii */
            post_game_info player;

            std::string line;
            char P;

            response >> player.game_time;

            std::getline(response, line);
            std::stringstream game_info(line);

            while(game_info >> P)
            {
                game_info >> player.ID;
                game_info >> player.player_time;
                game_info >> player.num_steps;
                game_info >> player.deaths;
                players_data.push_back(player);
            }
        }
        else if( response_code == GAME_OVER )
        {
            /* koniec hry */
            int num_winners, winner_id;

            response >>  num_winners;
            while(num_winners)
            {
                response >> winner_id;
                winners.push_back(winner_id);
                --num_winners;
            }

            game_is_runnig = false;
            return false;
        }
        else if( response_code == DISCONNECT || response_code == 599 )
        {
            /* klient odpojeny z hry alebo server neprijal prikaz */
            game_is_runnig = false;
            return false;
        }
        else
        {
            game_is_runnig = false;
            throw("game(): received unknown code. ");
        }
    }
    return true;
}





/**
 * @brief Pošle na server žiadosť o dostupné hry a spracuje výsledok
 *
 * Pridáva hry do Interface::available_games
 * @return Zoznam dostupných hier
 */
std::string Interface::getGames()
{
   using namespace std;
   string str, line, output;
   QEventLoop loop;
   int n,id;
   Info gameinfo;
   available_games.clear(); //po opakovanem zavolani musi odstranit puvodni informace
   
   QObject::connect(  socket, SIGNAL(readyRead()), &loop, SLOT(quit())  );

   socket->write("112\n");
   loop.exec();  // cakaj kym server neodpovie

   str = string(socket->readAll().constData()); // odpoved servra

   istringstream iss(str.c_str() );

   while( getline(iss, line ) ) // kazda hra je na novom riadku
   {

       str.clear();
       istringstream game_info(line.c_str());
       game_info >> n;

       if( n!= 512 )
           throw("getMaps: Wrong game format.");
       
        // id hry
       if(! (game_info >> id) ){
         break; //konec seznamu
       }
       gameinfo.set_id(id);

       game_info >> n; // pocet hracov
       gameinfo.set_num(n);

       game_info >> n; // max pocet hracov
       
       //nazev hry
       stringstream name;
       for(string s;game_info>>s;name<<s<<" ");//nacte zbytek radku
       
       gameinfo.set_name(name.str());

       available_games[id]=gameinfo;
       output += name.str() + "\n";
    }

    this->games(true);
    return output;
}



/**
 * @brief Pošle na server žiadosť o dostupné mapy a spracuje výsledok
 *
 * Pridava mapy do Interface::available_maps
 * @return Zoznam dostupných máp
 */
std::string Interface::getMaps()
{
    using namespace std;
    string str, line, output;
    QEventLoop loop;
    int n;
    bool done = false;
    
    available_maps.clear(); //po opakovanem zavolani musi odstranit puvodni informace
    
    QObject::connect(  socket, SIGNAL(readyRead()), &loop, SLOT(quit())  );

    Info mapinfo;
    socket->write("111 ");

    while( !done )
    {
        loop.exec(); // cakaj kym server neodpovie
        str += string(socket->readAll());

        if (str.find("511\n") != string::npos || str.find("511 \n") != string::npos )
            done = true;
    }

    istringstream iss(str.c_str() );

    while( getline(iss, line ) ) // kazda mapa je na novom riadku
    {

        if(line.empty() )
            continue;

        istringstream map(line);

        map >> n;

        if( n!= 511 )
            throw("getMaps(): Wrong map format.");

        map >> n; // id mapy
        mapinfo.set_id(n);

        istreambuf_iterator<char> eos;
        string name (istreambuf_iterator<char>(map), eos); // nazov mapy
        name.erase(0,1); // odstrani medzeru
        mapinfo.set_name(name);

        available_maps[n]=mapinfo;

        output += name + "\n";
     }

     this->maps(true);
     return output;
}





/**
 * @brief Realizuje prenos mapy zo serveru
 *
 * Metóda je používaná najmä pri vytváraní náhľadov máp v menu novej hry
 * @param map_id
 * @return Bludiste vytvorene z danej mapy
 */
Maze Interface::downloadMaze(int map_id = 0) const
{
    using namespace std;
    char data[BUFFER_SIZE];

    int bytes_read = 0;
    string map;

    QEventLoop loop;

    QObject::connect(socket, SIGNAL(readyRead()), &loop, SLOT(quit()));

    socket->write( "140 " );
    socket->write(to_string(map_id).c_str());

    loop.exec(); // cakaj na odpoved servra

    bytes_read = socket->readLine( data, BUFFER_SIZE );

    if( bytes_read == -1 )
        throw("downloadMaze(): readLine()");


    map = string(data);

    if( data[bytes_read-1] != '\n' ) // mapa neprisla cela
    {
        loop.exec(); // cakanie na zvysok mapy
        socket->readLine( data, BUFFER_SIZE );
        map += string(data);

    }

    istringstream iss(map.c_str());

    return this->parse_530(iss, map);
}



/**
 * @brief Metóda spracuje odpoveď s kódom 530 - pripojenie do hry/ založenie novej hry
 *
 * V rámci odpovede je prenášaná mapa hry, z ktorej sa skonštruuje bludisko
 * @return Bludisko, ktoré prišlo ako odpoveď
 * @param response Kompletný text správy
 * @param map Text správy ako std::string
 */
Maze Interface::parse_530( std::istringstream &response, std::string &map ) const
{
    using namespace std;

    int R,C,n;
    unsigned int header_len = 4; // velkost hlavicky odpovede, minimalne dlzka retazca "5x0 "
    vector<int> v;

    //qDebug() << "parse_530(): " << response.str().c_str();

    bool tmp (response >> n);
    if(!tmp)
    {
        throw("parse530(): Nothing to read.");
    }

    if( n == 530 )
    {
        response >> local_player_id; // ID hraca
        header_len += to_string(local_player_id).length() + 1; // +medzera
    }
    else if( n == 599 )
        throw("Servr nepodporuje nahled mapy.");

    else if( n == 559 )
        throw("Servr odmitnul pripojeni.");

    else if( n != 540 && n != 530 ) // kod odpovedi
    {
        qDebug() <<   "UNKNOWN CODE:" <<     n;
        throw("parse_530: not understood");
    }


    while (response >> n)
    {
      v.push_back(n);
      qDebug() << "parse_530: reading ROWS COLS:" << n;

    }


    if( v.size() == 2 ) // RxC
    {
        R = v.front();
        C = v.back();

        header_len += to_string(R).length() + 1 + to_string(C).length() + 1;

    }
    else
       throw("parse_530: Wrong response format");

    map.erase(0, header_len ) ; // odstranenie hlavicky z odpovede

    qDebug() << "MAPA:" << map.c_str();
    return Maze(R,C,map);
}




/**
 * @brief Vyhľadá ID danej mapy/hry medzi dostupnými mapami/hrami
 * @param name  Názov mapy/hry
 * @param games Prepínač medzi mapami a hrami
 * @throw - Výnimka, ak sa mapa nenájde
 * @return ID mapy/hry
 */
int Interface::name2id( const char* name, bool games ) const
{
    std::map<int,Info> data;
    if( games )
        data = this->available_games;
    else
        data = this->available_maps;

    for(unsigned int i = 0; i < data.size(); i++)
    {
       if(data.at(i).get_name().compare(name) == 0)
          return data.at(i).get_id();
    }
    throw("name2id: Map or game not found");
}




/**
 * @brief Vykoná príkaz
 *
 * Ak je input platnym príkazom hry, odošle sa na server
 * Ak je vstupom prázdny reťazec, príp. neoznačuje žiadny prikaz, metóda nespraví nič
 * @param input Vstupný reťazec
 */
void Interface::command( std::string input ) const
{
     if( local_player_killed )
         return;

     if(      input.compare("take")  == 0 )
     {
        qDebug() << "take";
        socket->write("154\n");
     }
     else if( input.compare("stop")  == 0 )
     {
        qDebug() << "stop";
        socket->write("151\n");
     }
     else if( input.compare("open")  == 0)
     {
        qDebug() << "open";
        socket->write("155\n");
     }
     else if( input.compare("left")  == 0)
     {
        qDebug() << "left";
        socket->write("152\n");
     }
     else if( input.compare("go")    == 0)
     {
        qDebug() << "go";
        socket->write("150\n");
     }
     else if( input.compare("right") == 0)
     {
        qDebug() << "right";
        socket->write("153\n");
     }
     else
         return;

}
























