/** @file interface.cpp
* @author Vladimír Čillo
* @brief Implementácia klientskej časti rozhrania medzi klientom a servrom
*
* Modul poskytuje triedy pre obsluhu herných udalostí na strane klienta
*/

#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <string>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QObject>
#include <QDebug>
#include "../shared/maze.hpp"
#include <vector>
#include <sstream>
#include <map>

/**
  * Maximálny počet hráčov v hre
  */
#define MAX_PLAYERS 4


/**
 * @brief Štruktúra reprezentujúca informácie o mape
 */
typedef struct {
    int id; ///< ID mapy
    char* name; ///< Meno mapy
} map_info;


/**
 * @brief Štruktúra reprezentujúca informácie o hráčovi potrebné po skončení hry
 */
typedef struct {
    int game_time; ///< Celkový čas hry
    int ID; ///< ID hráča
    int player_time; ///< Čas, ktorý hráč strávil v hre
    int num_steps; ///< Počet políčok, ktorými hráč prešiel
    int deaths; ///< Počet zabití hlídačom
} post_game_info;




/**
 * @brief Štruktúra reprezentujúca informácie o hráčovi potrebné počas hry
 *
 * Uchováva informácie o ID, pozícii a smere hráča
 */
typedef struct {
    int id;  ///< ID hráča
    union {  ///< X-ová pozícia
      int x;
      int col;
    };
    union { ///< Y-ová pozícia
      int y;
      int row;
    };
    int dir; ///< Smer, ktorým je hráč otočený
} player_info;




/**
 * @brief Trieda pre prácu s informáciami o hrách a mapách na strane klienta.
 *
 * Uchováva informácie o názve mapy/hry; ID mapy/hry a v prípade hry aj aktuálny počet hráčov v hre
 */
class Info
{
    int id; ///<  ID hry / mapy
    int num_players; ///<  Počet hráčov v hre
    std::string name; ///< Názov hry / mapy

public :
    int get_id() const { return id; }
    std::string &get_name() { return name; }
    int get_num() const { return num_players; }


    void set_id(int new_id) { id = new_id; }
    void set_num(int new_num ) { num_players = new_num; }
    void set_name( std::string  new_name ) { name = new_name; }
};


/**
 * @brief Singleton - Rozhranie medzi klientom a serverom
 *
 * Uchováva informácie o stave pripojenia, hrách a mapách dostupných na serveri, údaje o hráčoch, hlídačoch,
 * ich pozíciach a celkovom stave hry.
 *
 */
class Interface : public QObject
{

    Q_OBJECT
    static Interface* interf;  ///< Vlastný objekt interface

    QTcpSocket *socket; ///< Socket - spojenie so serverom
    Maze* map; ///< Mapa v práve prebiehajúcej hre

    std::map<int,Info> available_maps;  ///< Mapy dostupné na serveri
    std::map<int,Info> available_games; ///< Hry dostupné na serveri

    bool _init;  ///< Indikátor pripojenia na server
    bool _maps;  ///< True ak už server poslal dostupné mapy, zabraňuje viacnásobnému prenosu tejto informácie
    bool _games; ///< True ak už server poslal zoznam dostupných hier, zabraňuje viacnásobnému prenosu tejto informácie


    mutable int local_player_id; ///< ID lokálneho hráča
    player_info players[MAX_PLAYERS]; ///< Údaje o hráčoch v hre @see player_info
    int in_game_players; ///< Počet hráčov v aktuálne hranej hre
    std::vector<player_info> guards; ///< Údaje o hlídačoch v aktualne hranej hre
    bool game_is_runnig; ///< True ak hra beží, inak false


public:
    std::string response; ///< Informácie o úspešnosti príkazov
    std::vector<int> winners; ///< Zoznam ID výhercov hry (dĺžka 0-MAX_PLAYERS)
    std::vector<post_game_info> players_data; ///< Informácie o hráčoch v hre potrebné na konci (čas, počet políčok, atd.)
    bool local_player_killed; ///< Indikátor zabitia lokálneho hráča

    /**
     * @brief Metóda získa aktuálny stav hry
     * @return True ak hra beží, inak False
     */
    bool game_runnig() { return game_is_runnig; }


    /**
     * @brief Metóda pre prístup k informáciam o hlídačoch
     * @return Vektor guards @see player_info
     */
    std::vector<player_info>* getGuards(){ return &guards; }




    /**
     * @brief Metóda pre prístup k hráčovi
     * @param[in] index hráča ( @see {Interface::player_index()})
     * @return Referencia na hráča v poli hráčov @see Interface::players
     */
    player_info* get_player( int index )
    {
       if( index < 0 || index >= MAX_PLAYERS)
          return nullptr;
       else
          return &players[index];
    }
    

    /**
     * @brief Metóda pre prístup k lokálnemu hráčovi
     * @return Referencia na lokálneho hráča v poli hráčov @see Interface::players
     */
    player_info* get_local_player(){
      int ind=player_index(local_player_id);
      if(ind<0)
        return nullptr;
      return &players[ind];
    }


    /**
     * @brief Metóda pre získanie počtu hráčov v hre
     * @return Počet hráčov v hre
     */
    int get_in_game_players(){ return in_game_players; }



    /**
     * @brief Metóda pre odoslanie správy na server
     * @param Data, ktoré sa majú odoslať
     */
    void send(const char* data) { socket->write(data);}


    /**
     * @brief Načíta a vráti zoznam dostupných máp
     */
    std::map<int,Info> &getMaps2() {  
      getMaps();
      return available_maps; 
    }

    /**
     * @brief Načíta a vráti zoznam rozohraných hier
     */
    std::map<int,Info> &getGames2() {  
      getGames();
      return available_games; 
    }
    

    /**
     * @brief Nastaví hodnotu atribútu _maps na zadanú hodnotu
     * @param Nová hodnota atribútu _maps
     */
    void maps( bool value ) { _maps = value; }

    /**
     * @brief Vracia hodnotu atribútu _maps
     * @return Hodnota atribútu _maps
     */
    bool maps() const { return _maps; } // getter




    /**
     * @brief Nastaví hodnotu atribútu _init na zadanú hodnotu
     * @param  Nová hodnota atribútu _init
     */
    void init( bool value ) { _init = value; }


    /**
     * @brief Vracia hodnotu atribútu _init
     * @return Hodnota atribútu _init
     */
    bool init() const { return _init; }




    /**
     * @brief Nastaví hodnotu atribútu _games na zadanú hodnotu
     * @param  Nová hodnota atribútu _games
     */
    void games( bool value ) { _games = value; }

    /**
     * @brief Vracia hodnotu atribútu _games
     * @return Hodnota atribútu _games
     */
    bool games() const { return _games; }



    /**
     * @brief Nastaví mapu aktuálnej hry a zároveň vymaže starú
     * @param bludiste Mapa, ktorá sa bude hrať
     */
    void setMap( Maze bludiste ) { delete map; map = new Maze(bludiste);}


    /**
     * @brief Vracia počet riadkov aktuálne nastavenej mapy
     * @return Počet riadkov mapy
     */
    int Rows()  { return map->Rows(); }


    /**
     * @brief Vracia počet stĺpcov aktuálne nastavenej mapy
     * @return Počet stĺpcov mapy
     */
    int Cols()  { return map->Cols(); }


    /**
     * @brief Metóda pre prístup k bludisku
     * @return Referencia na data aktuálne nastavenej mapy
     */
    std::vector<Object>* getMaze() { return map->getMaze(); }


    /**
     * @brief Prístup k aktuálnej mape
     * @return Aktuálne nastavená mapa
     */
    Maze* get_current_map() { return map; }


    /**
     * @brief Metóda pre prístup k singletonu
     * @return Instancia interface
     */
    static Interface* getInstance()
    {
        if (interf == nullptr )
            interf = new Interface(0);

        return interf;
    }



    /* Metódy definované v interface.cpp */

    explicit Interface(QObject *parent);
    int name2id( const char* name, bool games ) const; // vrati id hry/ mapy podla mena
    void game_init( bool new_game,  int id = 0, const char* name = nullptr, double delay = 0 ); // funkcia volana pri spusteni hry
    bool game();
    void init(std::string host , int port);
    void readResponse();
    Maze downloadMaze(int map_id) const;
    void command( std::string input ) const;
    std::string getMaps();
    std::string getGames();
    int player_index( int player_id ) const;
    int guard_index(   int guard_id ) const;
    void clean_up();

private:
    Maze parse_530(std::istringstream &response , std::string &map) const;


};

#endif // INTERFACE_HPP
