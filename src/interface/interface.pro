#-------------------------------------------------
#
# Project created by QtCreator 2014-04-28T19:57:03
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = interface
TEMPLATE = lib
CONFIG += staticlib

include(../icp.pri)


SOURCES += interface.cpp

HEADERS += interface.hpp \
    ../shared/maze.hpp

