/**
 * @brief Třída pro klienta
 *
 * @file client.cpp
 * @author Vítězlsav Kříž - xkrizv01
 */
#include "client.hpp"
#include "boost/algorithm/string/trim.hpp"

using boost::asio::ip::tcp;


int Client::count = 0;

/**
 * @brief Příjme příkazy
 * 
 * Příjme všechny příkazy od klienta (příkazy jsou oddělené \n)
 * a nastrká je do fronty
 */
void Client::receive()
{
  if(crash)
    return;
  try
  {
    if (sock.available()){
      boost::asio::streambuf buf;
      tcp::iostream x;
      
      size_t n= sock.receive(buf.prepare(sock.available()));
      
      buf.commit(n);
      
      std::istream stream (&buf);
      
      
      std::string msg;
      while(std::getline(stream,msg)){
        boost::algorithm::trim_right(msg);
        commands.push(std::move(msg));
      }
    }
  }
  catch(std::exception e){
    crash=true; // neuspech, nastavi crash
  }
}


/**
 * @brief Odešle příkaz
 * 
 * Odešle jeden příkaz klientovy, přidá konec řádku
 * @param msg Prikaz
 */
bool Client::send(std::string msg)
{
  if(crash)
    return false;
  
  boost::asio::streambuf b;
  std::ostream os(&b);
  os << msg << "\n";
  
  size_t n;
  try{
  // try sending some data in input sequence
  n = sock.send(b.data());
  }
  catch(std::exception e){
    crash=true;
    return false;
  }

  b.consume(n);
  return true;
}


/**
 * @brief Bezpečne uzavře socket
 */
void Client::disconnect()
{
  try
  {
    sock.shutdown(tcp::socket::shutdown_both);
    sock.close();
  }
  catch(std::exception e){
    std::cerr<<e.what();
  }
}
