/**
 * @brief Třída pro klienta
 *
 * @file client.hpp
 * @author Vítězlsav Kříž - xkrizv01
 */
#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <string>
#include <queue>
#include <iostream>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;


/**
 * @brief Třída pro jednoho klienta
 * 
 * - Při vytvoření přiděluje unikátní id
 * - Poskytuje tcp::socket
 * - Zapouzdřuje odesílaní a příjímání příkazů
 * @warning NENÍ Thread Safe, pouze jedno vlákno může přistupovat
 */
class Client{
  static int count; ///< Číselník pro přidělování id
  int id; ///< Přidělené id
  /// Pokud se nepovede odeslání přijmutí nastaví se na true
  bool crash=false; 
  
public:
  tcp::socket sock; ///< socket s připojením
  std::queue<std::string> commands; ///< fronta příkazů
  
  /**
   * @brief Client konstruktor
   * 
   * Počítá počet vytvoření objektů, přiděluje id
   * @param io_service reference na boost::asio::io_service, nutné pro vytvoření soketu
   */
  Client(boost::asio::io_service &io_service): sock(io_service) {
    id=count++;
  }
  
  void receive();
  bool send(std::string msg);
  void disconnect();
  bool active(){
    if(crash)
      return false;
    return true;
  }
  
  
  int getId() const { return id; }
};

#endif // CLIENT_HPP
