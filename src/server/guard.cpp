/**
  * @brief Třídy s hlídači
  * 
  * @author Vítězlsav Kříž - xkrizv01
  * @file guard.cpp
  */
#include "guard.hpp"
#include <cstdlib>
#include <queue>
#include <map>


/**
 * @brief Provede jeden herní krok hlídače
 * 
 * Částečně kopíruje @ref PlayerLogic::gameTick, přidává navíc
 * zabíjení hráče
 */
void Guard::doStep()
{
  Object & next = game.getField(next_row(),next_col());
  if(next.is_occupied()){
    PlayerLogic* player = static_cast<PlayerLogic*>(next.get_host());
    player->kill();
  }
  if (state==GO){

    if(canEnter(next_row(),next_col())){
      game.getField(row,col).leave(this);
      game.getField(next_row(),next_col()).enter(this);
       
      x_change=true; //zmenila se pozice
      
      row=next_row();
      col=next_col();
      

      
      steps++;
    } else {
      state=STOP; //zastaveni hrace
      return;    
    }
  
  }
}


/**
 * @brief Určuje zda může hlídač vstoupit na políčko
 * 
 * Protože hlídač může vstoupit na políčko s hráčem, tak
 * je nutné použít vlastní metodu, nestačí @ref Object::can_enter
 * Metoda je virtuální, umožňuje nahrazení ve zděděných třídách hlídaču.
 * Tím umožňuje měnit chování, bez změny celého algoritmu @see GuardCheater
 * @param r Řádek
 * @param c Sloupec
 * @return true = může vstoupit
 */
bool Guard::canEnter(int r, int c)
{
  bool a,b;
  
  switch(game.getField(r,c).get_type()){
    case Object::GATE:
      if(game.getField(r,c).is_open())
        a=true;
      else
        a=false;
      break;
    case Object::WALL:
      a=false;
      break;
    default:
      a=true;
  }
  b=true;
  if(game.getField(r,c).is_occupied())
    b=static_cast<PlayerLogic*>(game.getField(r,c).get_host())->is_mortal();
  
  return a && b;
}

/**
 * @brief Mění směr a stav hlídače
 * 
 * Na rozdíl od @ref PlayerLogic::gameTick neovládá samotný pohyb,
 * o ten se stará @ref doStep.
 * 
 * Tento hlídač pouze pochoduje tam a zpátky v prvním volném směru, který našel
 */
void Guard::gameTick()
{
  if(!canEnter(next_row(),next_col())){
    left();
    left();
  }
    
  go();
  

  doStep();
  
}

/**
 * @brief Mění směr a stav hlídače.
 * @see @Guard::gameTick
 * 
 * Tento hlídač se neustále pohybuje, mění náhodně směr pokud může.
 * Pokud to není nutné, tak se nevrací zpět.
 */
void GuardRand::gameTick()
{
  
  for(int i=0;i<8;++i){
    int n = std::rand()%3;
    if(n==0){
      left();
      if(canEnter(next_row(),next_col()))
        break;
      right();
    }
    else if(n==1){
      right();
      if(canEnter(next_row(),next_col()))
        break;
      left();
    } else {
      if(canEnter(next_row(),next_col()))
        break;
    }
  }
  
  if(!canEnter(next_row(),next_col())){
    left();
    left();
  }
  
   go();
 
   doStep();  
}

/**
 * @brief Vypočítá potřebné kroky k nalezení hráče
 * 
 * Prohledává do vzdálenosti podle @ref BFSDepth. Nalezenou cesstu uloží do
 * fronty @ref nextCmds.
 * @return true = nalezl heáče, false = neúspěch
 */
bool GuardBFS::next_steps()
{
  std::map<int,node> closed;
  std::queue<node> open;
  nextCmds.clear();
  closed[rctox(row,col)]=node();
  
  
  // prvni krok do ctyr smeru
  if(canEnter(row-1,col))
    open.push(node(row-1,col,1,N,rctox(row,col)));
  if(canEnter(row,col+1))
    open.push(node(row,col+1,1,E,rctox(row,col)));
  if(canEnter(row+1,col))
    open.push(node(row+1,col,1,S,rctox(row,col)));
  if(canEnter(row,col-1))
    open.push(node(row,col-1,1,W,rctox(row,col)));
  
  // dokud neni fronta prazdna nebo je vzdálenost větší
  while (!open.empty() && open.front().len<BFSDepth){
    Object &field=game.getField(open.front().row,open.front().col);
    PlayerLogic * host = static_cast<PlayerLogic*>(field.get_host());
    
    //pridani do seznamu closed, policko se jiz nenavstevuje
    closed[rctox(open.front().row,open.front().col)]=open.front();
    
    //pokud je nekdo na policku, a pokud je to smrtelny hrac
    //konci prohledavani a ulozi si cestu
    if(host!=nullptr){
      if(host->is_mortal()){
        for(int x=rctox(open.front().row,open.front().col);x!=rctox(row,col);x=closed[x].prev){
          nextCmds.push_front(closed[x].dir);
        }
        return true;
      }
    }
    
    //projde vsechny smery
    for(int i=0;i<4;++i){
      int r=open.front().row,c=open.front().col;
      t_direction d;
      switch(i){
        case 0:
          r++;
          d=S;
          break;
        case 1:
          c++;
          d=E;
          break;
        case 2:
          r--;
          d=N;
          break;
        case 3:
          c--;
          d=W;
          break;
        default:
          break;
      }
      //pokud jde vstoupit a pokud jiz neprohledal tak prida do fronty na prohledavani
      if(canEnter(r,c) && closed.find(rctox(r,c))==closed.end()){
        open.push(node(r,c,open.front().len+1,d,rctox(open.front().row,open.front().col)));
      }
    }

    open.pop();
  }
  
  
  return false;
}

/**
 * @brief Převádí požadovaný směr na posloupnost příkazu left
 * @param set_dir směr N,E,S,W
 */
void GuardBFS::dirToCmd(AbstractPlayer::t_direction set_dir)
{
  while(direction!=set_dir){
    left();
  }
}

/**
 * @brief Otočí hráče podle nalezené cesty
 * 
 * Vybírá příkazy které nalezl v @ref next_steps.
 * Pokud nemá již žádné další hledá znovu
 */
void GuardBFS::rotate()
{
  t_direction next=N;
  
  if(wait==0){
    if(nextCmds.empty()){
      if(!next_steps()){
        wait=wait_setting;
      } else {
        next=nextCmds.front();
        nextCmds.pop_front();
      }
    } else {
      next=nextCmds.front();
      nextCmds.pop_front();
    } 
    
    dirToCmd(next);   
    
    
    if(!canEnter(next_row(),next_col())){
      if(!next_steps()){
        wait=wait_setting;
      } else {
        next=nextCmds.front();
        nextCmds.pop_front();
      }
    }
    
    dirToCmd(next);
    //std::cout << "stack" << nextCmds.size() << " dir:" << playerIcon(next) << "\n";
    go();
    
  } else {
    stop();
    wait--;
  }
}

/**
 * @brief Otočí hlídače a udělá krok
 */
void GuardBFS::gameTick()
{
   rotate();
     
   doStep();  
}

/**
 * @brief Test zda může hlídač vstoupit na políčko
 * 
 * Rozličuje kolik klíčů má hlídač, a zavřeno bránu bere jako políčko
 * kde jde vstoupit
 * @param r Řádek
 * @param c Sloupec
 * @return true = může vstoupit
 */
bool GuardCheater::canEnter(int r, int c)
{
  bool a,b;
  
  switch(game.getField(r,c).get_type()){
    case Object::GATE:
      if(game.getField(r,c).is_open())
        a=true;
      else
        a=key>0; // pokud ma klic
      break;
    case Object::WALL:
      a=false;
      break;
    default:
      a=true;
  }
  b=true;
  if(game.getField(r,c).is_occupied())
    b=static_cast<PlayerLogic*>(game.getField(r,c).get_host())->is_mortal();
  
  return a && b;
}

/**
 * @brief Jeden krok hlídače
 * 
 * Otočí se podle vyhledávacího algoritmu, otevře bránu, udělá krok.
 */
void GuardCheater::gameTick()
{
   
   rotate();
   
   if(game.getField(next_row(),next_col()).get_type()==Object::GATE)
     open_gate();
   
   doStep(); 
}
