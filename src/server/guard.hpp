/**
  * @brief Třídy s hlídači
  * 
  * @author Vítězlsav Kříž - xkrizv01
  * @file guard.hpp
  */

#ifndef GUARD_HPP
#define GUARD_HPP

#include "servermaze.hpp"
#include "playerlogic.hpp"

#include <iostream>

/**
 * @brief Základní hlídač
 * 
 * Dědí od @ref PlayerLogic, implementuje stejnou virtualní metodu
 * @ref gameTick. Celé ovládání hlídače spočívá ve volání této metody.
 * 
 * Tento hlídač pouze pochoduje tam a zpátky v prvním volném směru, který našel.
 */
class Guard : public PlayerLogic
{
protected:
  void doStep();
  virtual bool canEnter(int r,int c);
public:
  Guard(ServerMaze & setgame,int r,int c): PlayerLogic(setgame){
    game.getField(row,col).leave(this);
    x_is_mortal=false;
    row=r;
    col=c;
    game.getField(row,col).enter(this);
    for(int i=0;!canEnter(next_row(),next_col()) && i<4;i++){
      left();
    }
  }
  ~Guard(){}
  void gameTick();

  bool kill() {return false;}
};

/**
 * @brief Hlídač s náhodným pohybem
 */
class GuardRand : public Guard
{
  bool canEnter(int r, int c) {return Guard::canEnter(r,c);}
public:
  GuardRand(ServerMaze & setgame,int r,int c): Guard(setgame,r,c) {}
  ~GuardRand(){}
  void gameTick();
  bool kill() {return false;}
};

/**
 * @brief Hlídač s vyhledávacím algoritmem
 */
class GuardBFS : public Guard
{
protected:  
  int BFSDepth=10;
  int wait_setting=5;
  int wait=0;
  bool next_steps();
  void dirToCmd(t_direction set_dir);
  void rotate();
  bool canEnter(int r, int c) {return Guard::canEnter(r,c);}
  int rctox(int r,int c){
    return r*1000+c;
  }
  
  struct node{
    int row;
    int col;
    int len;
    t_direction dir;
    int prev;
    node(int r,int c,int l,t_direction d): row(r),col(c),len(l),dir(d) {}
    node(int r,int c,int l,t_direction d,int pr): row(r),col(c),len(l),dir(d),prev(pr) {}
    node(){}
  };
  
  std::deque<t_direction> nextCmds;
  
public:
  GuardBFS(ServerMaze & setgame,int r,int c): Guard(setgame,r,c) {}
  ~GuardBFS(){}
  void gameTick();
  bool kill() {return false;}
  void setSmart(int range,int sleep) { 
    BFSDepth=range;
    wait_setting=sleep;
  }
};

/**
 * @brief Hlídač s vyhledávacím algoritmem, a schopností odemykat brány
 */
class GuardCheater: public GuardBFS
{
protected:
  bool canEnter(int r, int c);
public:
  GuardCheater(ServerMaze & setgame,int r,int c): GuardBFS(setgame,r,c) {
    BFSDepth=50;
    key=10;
  }
  ~GuardCheater(){}
  void gameTick();
  bool kill() {return false;}
  void setKey(int k) {key=k;}
};


#endif // GUARD_HPP
