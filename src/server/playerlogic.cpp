/**
 * @brief Třída pro hráče na serveru
 * 
 * @author Vítězlsav Kříž - xkrizv01
 * @file playerlogic.cpp
 */
#include "playerlogic.hpp"
#include <queue>



/**
 * @brief Umisteni hrace do mapy
 * 
 * Nejprve projde všechny uložené pozice s místy pro umístění
 * hráču (@ref ServerMaze::player_spawns). Poté prochází sekvenčně
 * celé bludiště. Na první volnou pozici umístit hráče.
 */
void PlayerLogic::spawn(){
  row=-1;
  col=-1;
  x_change=true; //zmenila se pozice
  for(auto pos:game.player_spawns){
    Object & field= game.getField(pos.row,pos.col);
    if (field.can_enter()){
      field.enter(this);
      row=pos.row;
      col=pos.col;
      return;      
    }
  }
  
  
  // spawnuti na libovolnou  volnou pozici 
  for(row=0;row<game.Rows();++row)
    for(col=0;col<game.Cols();++col){
      Object & field= game.getField(row,col);
      if (field.can_enter()){
        field.enter(this);        
        return;      
      }
    }
}


/**
 * @brief Zahodí klíče
 * 
 * Současná implementace zahodí pouze jeden klíč.
 */
void PlayerLogic::dropKeys()
{
    if(key>0 && game.getField(row,col).placeKey()){
      game.changes.push(std::pair<int,int>(row,col)); 
      key--;
    }

}

/**
 * @brief Zabití hráče hlídačem
 * @return true = úspěch
 */
bool PlayerLogic::kill()
{
  state=DEAD;
  x_change=true;
  game.getField(row,col).leave(this);
  deaths++;
  dropKeys();
  return true;
}



/**
 * @brief Provede jeden herni krok hrace
 */
void PlayerLogic::gameTick(){
  
  if(game.getField(row,col).get_type()==Object::GOAL){
    game.end=true;
    state=STOP;//aby ten cil nahdou nepresel
  }
  
  if (state==GO){
    
    //pickup_key();
    //open_gate();
    // zvedani klice musi byt explicitni
    
    if(game.getField(next_row(),next_col()).can_enter()){
      game.getField(row,col).leave(this);
      game.getField(next_row(),next_col()).enter(this);
      
      
      x_change=true; //zmenila se pozice
      
      row=next_row();
      col=next_col();
      

      
      steps++;
    } else {
      state=STOP; //zastaveni hrace
      return;    
    }
  
  }
  
  
}

/**
 * @brief Provede zvednuti klice
 * 
 * z policka ktere se nachazi pred hracem
 * @return Uspech/neuspech
 */
bool PlayerLogic::pickup_key(){
  if(state == DEAD || state == DISCONNECTED)
    return false;
  Object & next=game.getField(next_row(),next_col());
  if(next.get_type()==Object::KEY && next.action()){
    // uchovani zmen
    game.changes.push(std::pair<int,int>(next_row(),next_col()));
    key++;
    return true;
  } else {
    return false;
  }
}

bool PlayerLogic::open_gate()
{
  if(state == DEAD || state == DISCONNECTED)
    return false;
  Object & next=game.getField(next_row(),next_col());
  if(key>0 && next.get_type() == Object::GATE){
    if(next.action()){
      game.changes.push(std::pair<int,int>(next_row(),next_col()));
    
      key--;
    }
    return true;
  } else {
    return false;
  }  
}



