/**
 * @brief Třída pro hráče na serveru
 * 
 * @author Vítězlsav Kříž - xkrizv01
 * @file playerlogic.hpp
 */

#ifndef PLAYERLOGIC_H
#define PLAYERLOGIC_H

#include "../shared/abstractplayer.hpp"
//#include "../shared/maze.hpp"
#include "servermaze.hpp"
#include <ctime>

/**
 * @brief Třída pro hráče s herní logikou.
 * 
 * Využití na serveru pro ovládání hráče.
 */
class PlayerLogic :public  AbstractPlayer
{
public:
  /** Typ pro stav hrace ve hre */
  enum t_state {
    GO, ///< Hrac jde, dokud nenarazi na prekazku
    STOP,///<Hrac stoji na miste
    DEAD,///<Hrac je mrtvy
    DISCONNECTED,///<Hrac je odpojeny
  };
  
protected:  
  ServerMaze &game; ///< Reference na hraci plochu
  
  t_state state; ///< Aktualni stav hrace
  int key=0; //   ma hrac klic ? // int muze mit vice klicu?
  
  //struct timeval entered_game_at; // kdy vstoupil do hry
  int steps=0; // kolika policky hrac prosel
  
  void spawn();
  
  inline int next_row();
  inline int next_col();
  bool x_change = true;
  bool x_is_mortal;
private:
  void dropKeys();
  time_t startTime;
  int deaths=0;
public:
  
  
  PlayerLogic(ServerMaze & setgame): game(setgame), state(STOP) {
    std::time(&startTime);
    spawn();
    x_is_mortal=true;
  }
  virtual ~PlayerLogic(){}
  bool pickup_key();
  bool open_gate();
  inline bool left();
  inline bool right();
  inline bool go();
  inline bool stop();
  virtual bool kill();
  bool is_mortal(){
    return x_is_mortal;
  }
  
  inline void respawn();
  
  void leave() {
    state=DISCONNECTED;
    x_change=true;
    game.getField(row,col).leave(this);
  }
  
  inline int protokolState();
  
  t_state getState() const{return state;}
  int getSteps() const {return steps;}
  time_t getStartTime() const {return startTime;}
  int getDeaths() const {return deaths;}
  virtual void gameTick();
  inline bool changed();
  
  
};


/**
 * @brief Zmenil se stav hrace?
 * 
 * Pokud se zmenil stav hrace vraci true
 * Vedlejsi efekt, zrusi informaci o zmene
 * Opakovane volani nemusi vratit stejny vysledek
 * @return true ,pokud se zmenil stav
 */
bool PlayerLogic::changed(){
  if (x_change){
    x_change=false;
    return true;
  } else
    return false;
}


/**
 * @brief Další řádek v závislosti na směru
 */
int PlayerLogic::next_row(){
    switch (direction){
      case S:
        return row + 1;
      case N: 
        return row - 1;
      default:
        return row;
    }  
}

/**
 * @brief Další sloupec v závislosti na směru
 */
int PlayerLogic::next_col(){
  switch (direction){
    case E:
      return col + 1;
    case W: 
      return col - 1;
    default:
      return col;
  }  
}



/**
 * @brief Otočí hráče doleva
 * @return Uspěch = true
 */
inline bool PlayerLogic::left(){
  if(state == DEAD || state == DISCONNECTED)
    return false;
  state=STOP;
  x_change=true;
  switch (direction){
    case N: direction=W; break;
    case E: direction=N; break;
    case S: direction=E; break;
    case W: direction=S; break;
  }
  return true;
}

/**
 * @brief Otočí hráče doprava
 * @return Uspěch = true
 */
inline bool PlayerLogic::right(){
  if(state == DEAD || state == DISCONNECTED)
    return false;
  state=STOP;
  x_change=true;
  switch (direction){
    case N: direction=E; break;
    case E: direction=S; break;
    case S: direction=W; break;
    case W: direction=N; break;
  }
  return true;
}

/**
 * @brief Změní stav hráče na @ref GO
 * @return Uspěch = true
 */
inline bool PlayerLogic::go(){
  if(state == DEAD || state == DISCONNECTED)
    return false;
  state=GO;
  return true;
}

/**
 * @brief Změní stav hráče na @ref STOP
 * @return Uspěch = true
 */
inline bool PlayerLogic::stop(){
  if(state == DEAD || state == DISCONNECTED)
    return false;
  state=STOP;
  return true;
}

/**
 * @brief Umisti hrace na vychozi pozici
 */
void PlayerLogic::respawn()
{
  state=STOP;
  spawn();
}


/**
 * @brief Převádí vnitřní stav na hodnotu stavu v protokolu
 * @return ALIVE = 0, DEAD = 1, DISCONNECT = 2
 */
int PlayerLogic::protokolState()
{
  if (state==DEAD)
    return 1;
  else if (state==DISCONNECTED)
    return 2;
  else
    return 0;
}

#endif // PLAYERLOGIC_H
