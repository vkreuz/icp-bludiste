/**
 * @brief Třídy pro místnosti (obsluhu klientů)
 * 
 * @author Vítězlsav Kříž - xkrizv01
 * @file room.cpp
 */

#include "room.hpp"

#include <thread>
#include <map>
#include <fstream>
#include <chrono>
#include <vector>



std::map<int,Room*> rooms;

/**
 * @brief Zašle zprávu všem připojeným klientům
 * 
 * Používá mutex a lock, takže by to mělo být Thread Safe
 * @param msg Zpráva
 */
void Room::sendAll(std::string msg)
{
  cli_lock.lock();
  for(auto cli : clients){
    cli->send(msg);
  }
  cli_lock.unlock();
}

/**
 * @brief Smyčka Room
 * 
 * Pouze zkušební implemtace
 * Zasílá vše klientům zprávu "loop".
 */
bool Room::loop()
{
  cli_lock.lock();
  if(checkStop()) {
    cli_lock.unlock();
    return false;
  }
  sendAll("loop");
  cli_lock.unlock();
  return true;
}

Room::~Room()
{
  for(auto x:clients){
    delete x;
  }
}



/**
 * @brief Pripoji klienta do mistnosti
 * @param cli Ukazatel na Client
 */
void Room::connectClient(Client *cli)
{
  cli_lock.lock();
  clients.push_back(std::move(cli));
  cli_lock.unlock(); 
}


/**
 * @brief Hlavni cyklus vstupni (neherni) mistnosti
 * 
 * Zajistuje obsluhu klientu pripojenych k serveru
 * Poskytuje jim seznam map, her
 * Umoznuje pripojit ke hre nebo zalozit novu
 * Primo implementuje komunikacni protokol
 */
bool LobbyRoom::loop()
{
  cli_lock.lock();
  if(checkStop()) {
    cli_lock.unlock();
    return false;
  }
  
  for(auto it = clients.begin(); it!=clients.end();++it){
    Client* cli = *it;
    cli->receive();
    
    // odpojeni neaktivnich
    if(!cli->active()){
      cli->disconnect();
      it=clients.erase(it);
      delete cli;
      if(it==clients.end())
        break;
      else
        continue;
    }
    
    // zpracovani prikazu
    while(!cli->commands.empty()){
      std::stringstream cmd (cli->commands.front()); 
      std::stringstream msg;
      cli->commands.pop();
      int code=0;
      cmd >> code;
    
      if (code==195){ ///< ByeBye
        cli->disconnect();
        it=clients.erase(it);
        delete cli;
        break;
      } else {
        switch(code){
          case 100: ///< Welcome message
            msg << "500 " << cli->getId();
            break;
          case 111: ///< List of maps
            for(unsigned i=0;i<maps.size();++i){
              msg << "511 " << i << " " << maps[i].first << "\n";
            }
            msg << "511 ";
            break;
          case 112: ///< List of games
            for(unsigned i=0;i<games.size();++i){  
              msg << "512 " 
                  <<  i << " " 
                   << games[i]->countP() << " "  
                   << games[i]->maxP() << " " 
                   << games[i]->name << "\n";
            }
            msg << "512 ";
            break;
          case 120: ///< Create game and connect
            {
              unsigned id_map;
              int max_players;
              double delay;
              std::stringstream name;
              if(cmd >> id_map >> delay >> max_players){
                if(id_map>=maps.size()){
                  msg << "559 BAD ID_MAP";
                } else {
                  for(std::string s;cmd>>s;name<<s<<" ");//nacte zbytek radku
                  
                  GameRoom * game= createGame(id_map,delay,name.str(),max_players);
                  
                  game->connectClient(cli);
                  
                  it=clients.erase(it);
                  
                }
              }
              else {
                msg << "559 BAD FORMAT";
                //std::cerr << msg.str() << std::endl << cmd.str();
              }
            }
            break;
          case 130: ///< Connect
            unsigned id_game;
            if (cmd >> id_game){
              if (id_game>=games.size()){
                msg << "559 BAD ID_GAME";
                break; 
              } else {
                it=clients.erase(it);
                
                games[id_game]->connectClient(cli);  
              }
            } else  
              msg << "559 BAD FORMAT ";
            break;
          case 140: ///< Send map data
            {
              unsigned i;
              cmd >> i;
              
              msg << "540 " 
                  << maps[i].second.Rows() << " " 
                  << maps[i].second.Cols() << " " 
                  << maps[i].second.toString();
            }
            break;
          case 999:
            std::exit(42);
          default:
            msg << "599 COMMAND NOT RECOGNIZED : '" << cmd.str() << "'";
            
        }
        cli->send(msg.str());
      }
    }
    if(it==clients.end()) break; //kvuli pripadnemu mazani
  }
  cli_lock.unlock();
  
  
  // odstranuje dohrane hry
  for(auto it=games.begin();it!=games.end();){
    GameRoom *x=*it;
    if(x->canRemove()){
      it=games.erase(it);
      delete x;
    } else {
      it++;
    }
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(delay));
  return true;
}

/**
 * @brief Vytvoří novou hru a přidá ji do seznamu
 * @param id_map id mapy, podle ktere vytvori novou hru
 * @param delay  zpozdeni jednoho kroku
 * @param name   Jmeno hry
 * @param max_players Maximalní počet hráču ve hře
 * @return Ukazatel na vytvořenou herní místnost
 */
GameRoom *LobbyRoom::createGame(int id_map, double delay, std::string name, int max_players){
  GameRoom *game= new GameRoom(maps[id_map].second,delay,name,max_players);
  addGame(game);
  return game;
}

/**
 * @brief Konstruktor LobbyRoom
 * 
 * Načítá všechny mapy ze souboru ./maps/maps do paměti
 *
 */
LobbyRoom::LobbyRoom()
{
  delay=100;
 
  std::ifstream stream ("maps/maps");
  std::string line;
  std::string filename;
  while(std::getline(stream,line)){
    filename="maps/"+line;
    try{
      maps.push_back(std::pair<std::string,ServerMaze>(line,ServerMaze(filename.c_str())));
    }
    catch(const char *e){
      std::cerr << "Nepovedlo se nacist mapu:'" << filename << "'  Chyba: " << std::string(e) << std::endl;
    }
  }
}

LobbyRoom::~LobbyRoom()
{
  for(auto x:games){
    delete x;
  }
  games.clear();
}

/**
 * @brief Pridani hry na server
 * 
 * Prida hru do ridici mistnosti (LobbyRoom) a spusti vlakno
 * s novou hrou.
 * @param game Ukazatel na vytvorenou hru
 */
void LobbyRoom::addGame(GameRoom *game)
{
  
  std::thread t (roomLoop,game);
  games.push_back(std::move(game));
  t.detach();
}

/**
 * @brief Pripoji klienta do mistnosti
 * @param cli Ukazatel na Client
 */
void LobbyRoom::connectClient(Client *cli)
{
  cli_lock.lock();
  clients.push_back(std::move(cli));
  cli_lock.unlock();
}




/**
 * @brief GameRoom::doOneClient
 * 
 * Zpracuje prikazy od jednoho klienta
 * @param it iterator na clienta
 * @return upraveny iterator
 */
std::list<Client*>::iterator GameRoom::doOneClient(std::list<Client*>::iterator it)
{
  Client* cli = *it;
  cli->receive();
  int player_id = client_player[cli->getId()];
  PlayerLogic *player=players[player_id];
  
  if(!cli->active()){
    player->leave();
    it=clients.erase(it);
    client_player.erase(cli->getId());
    delete cli;
    return it;
  }
  
  while(!cli->commands.empty())
  {
    std::stringstream cmd (cli->commands.front());
    cli->commands.pop();
    std::stringstream msg; 

    int code;
    cmd >> code;
    
    if(plan.end)
      if(code!=199){
        msg << "700 " << (code-150) << " 1";
        code=0;
      }
    
    switch (code){
      case 150:
        msg << "700 " << (code-150) << " " << ! player->go();
        break;
      case 151:
        msg << "700 " << (code-150) << " " << ! player->stop();;
        break;
      case 152:
        msg << "700 " << (code-150) << " " << ! player->left();;
        break;
      case 153:
        msg << "700 " << (code-150) << " " << ! player->right();;
        break;
      case 154:
        msg << "700 " << (code-150) << " " << !player->pickup_key();
        break;
      case 155:
        msg << "700 " << (code-150) << " " << !player->open_gate();
        break;
      case 156: ///< START
        
      case 157: ///< PAUSE
        msg << "599 COMMAND NOT RECOGNIZED : '" << cmd.str() << "'";
        break;
      case 199: ///< LEAVE GAME
        it=clients.erase(it);
        player->leave();
        client_player.erase(cli->getId());
        cli->send("501 ");
        LobbyRoom::GetInstance().connectClient(cli);
        return it;
      default:
        break;
    }
    cli->send(msg.str());
  }
  return it;
} 



/**
 * @brief Smycka pro kazdou hru
 * 
 * Obsahuje v kazde iteraci zpozdeni podle hodnoty delay
 * Obsluhuje hrace, provadi herni prikazy, zasila zpet zmeny
 * mapy.
 */
bool GameRoom::loop()
{
  cli_lock.lock();
  if(checkStop()) {
    cli_lock.unlock();
    return false;
  }
  
  //obslouzi prikazy hracu
  for(auto it = clients.begin(); it!=clients.end();++it){
    it=doOneClient(it);
    if(it==clients.end()) break;
  }        
  
  if(plan.end){
    endGame();
    return true;
  }
    
  
  //pohne s hraci
  for(auto x:players){
    PlayerLogic *player=x.second;
    player->gameTick();
  }
  //pohne s hlidaci
  for(Guard* x: guards){
    x->gameTick();
  }

  std::stringstream msg;
  
  //zasle zmeny hracu
  for(auto x:players){
    PlayerLogic * player = x.second;
    if (player->changed())
    {
      msg << "P" << " " 
          <<  x.first  << " " 
          <<  player->getRow()  << " " 
          <<  player->getCol()  << " "
          << player->get_dir()  << " "
          << player->protokolState()  << " ";
    }
    if(player->getState()==PlayerLogic::DEAD ){
      player->respawn();
    }
    if(player->getState()==PlayerLogic::DISCONNECTED){
      players.erase(x.first);
      delete player;
    }

  }
  
  //zasle zmeny hlidacu
  int i=0;
  for(Guard* x:guards){
    i++;
    if (x->changed())
    {
      msg << "H" << " " 
          <<  i  << " " 
          <<  x->getRow()  << " " 
          <<  x->getCol()  << " "
          << x->get_dir()<< " ";
    }
  }  
  
  //zasle zmeny mapy
  while(!plan.changes.empty()){
    int x=plan.changes.front().first;
    int y=plan.changes.front().second;
    plan.changes.pop();
    Object & obj = plan.getField(x,y);
    
    switch (obj.get_type()){
      case Object::GATE:
        msg << "G" << " " 
            << x << " " 
            << y << " ";
        /*if (obj.is_open())
          msg << 0;
        else
          msg << 1;*/
        msg << " ";
      break;
      case Object::KEY:
        msg << "K" << " " 
            << x << " " 
            << y << " "
            << 1 << " ";
      break;
      case Object::EMPTY:
        msg << "K" << " " 
            << x << " " 
            << y << " "
            << 0 << " ";
      break;
      default:
        break;
    }
  }
  
  if(!msg.str().empty())
    sendAll("550 "+msg.str());
  
  
//  plan.Print();
  
  if(countP()==0){
    doStop();
  }
  
  cli_lock.unlock();
  std::this_thread::sleep_for(std::chrono::milliseconds(delay));
  return true;
}

/**
 * @brief Umístit na mapu hlídače
 * 
 * Hlídači jsou ruzných obtížností 1-9
 */
void GameRoom::spawnGuards()
{
  for(auto spw:plan.guard_spawns){
    Guard * g;
    switch(spw.level){
      case 1: // Nejlehčí - pouze pochoduje tam a zpět
        g=new Guard(plan,spw.row,spw.col);
        break;
      case 2: // Codí od zdi ke zdi - otočí se náhodným směrem
        g=new GuardRand(plan,spw.row,spw.col);
        break;
      case 3: // Hledá hráče do vzdálenosti 3 
        g=new GuardBFS(plan,spw.row,spw.col);
        static_cast<GuardBFS*>(g)->setSmart(3,0);
        break;
      case 4: // Hledá hráče do vzdálenosti 10 
        g=new GuardBFS(plan,spw.row,spw.col);
        static_cast<GuardBFS*>(g)->setSmart(5,3);
        break;
      case 5:
        g=new GuardBFS(plan,spw.row,spw.col);
        static_cast<GuardBFS*>(g)->setSmart(8,5);
        break;
      case 6:
        g=new GuardBFS(plan,spw.row,spw.col);
        static_cast<GuardBFS*>(g)->setSmart(15,20);
        break;
      case 7: // Má jeden klíč kterým si umí otevřít bránu
        g=new GuardCheater(plan,spw.row,spw.col);
        static_cast<GuardCheater*>(g)->setSmart(5,3);
        static_cast<GuardCheater*>(g)->setKey(1);
        break;
      case 8: // Má 10 klíčů
        g=new GuardCheater(plan,spw.row,spw.col);
        static_cast<GuardCheater*>(g)->setSmart(8,5);
        static_cast<GuardCheater*>(g)->setKey(10);
        break;
      case 9: // 100 klíčů, velký dohled
        g=new GuardCheater(plan,spw.row,spw.col);
        static_cast<GuardCheater*>(g)->setSmart(15,15);
        static_cast<GuardCheater*>(g)->setKey(100);
        break;
      default:
        g=new GuardRand(plan,spw.row,spw.col);
    }
    guards.push_back(std::move(g));
  }
}

/**
 * @brief Ukončí hru a zašle informace o výsledku
 */
void GameRoom::endGame()
{
  std::ostringstream msg;
  std::vector<int> winners;
  if(!gameEnded) //zabrání opakovanému volání
  {
    delay=100;
    gameEnded=true;
    
    time_t now;
    std::time(&now);
    
    msg << "600 " 
        << std::difftime(now,startTime) << " ";
    //zasle zmeny hracu
    for(auto x:players){
      PlayerLogic * player = x.second;  
        msg << "P" << " " 
            <<  x.first  << " " 
            <<  std::difftime(now,player->getStartTime())  << " " 
            <<  player->getSteps()  << " "
            <<  player->getDeaths() << " ";
        if(plan.getField(player->getRow(),player->getCol()).get_type()==Object::GOAL){
          winners.push_back(x.first);
        }
    }  
    msg << "\n";
    msg << "800 " << winners.size();
    for(auto id:winners){
      msg << " " << id;
    }
    sendAll(msg.str());
    
    for(auto it = clients.begin(); it!=clients.end();++it){
      
      Client* cli = *it;
      LobbyRoom::GetInstance().connectClient(cli);
    }
    clients.clear();
    doStop();
   
  }
}

GameRoom::~GameRoom()
{
  for(auto x:players)
    delete x.second;
  players.clear();
  for(auto x:guards)
    delete x;
  guards.clear();
}




/**
 * @brief Pripoji klienta do herni mistnosti
 * 
 * Vytvori noveho hrace, Zasle klientovi mapu a zmeny mapy
 * @param cli Ukazatel na Client
 */
void GameRoom::connectClient(Client *cli)
{
  if(checkStop()){
    cli->send("559 GAME IS ENDED");
    LobbyRoom::GetInstance().connectClient(cli);
    return;
  }
  
  cli_lock.lock();
  if(players.size() >= maxPlayers){
    cli->send("559 MAX PLAYER LIMIT");
    LobbyRoom::GetInstance().connectClient(cli);
  } else {  
    clients.push_back(std::move(cli));
    unsigned i;
    for(i=0;i<maxPlayers;++i){
      if(players.find(i)==players.end())
        break;
    }
    
    client_player[cli->getId()]=i;
    
    players[i]= new PlayerLogic(plan);
    
    std::stringstream msg;
    msg << "530" << " " 
        << i << " " 
        << plan.Rows() << " " 
        << plan.Cols() << " " 
        << plan.toString();
    
    
    
    // zaslani vsech pozic
    msg << "\n550 "; // new message
    
    for(auto x:players){
      PlayerLogic * player = x.second;
      msg << "P" << " " 
          <<  x.first  << " " 
          <<  player->getRow()  << " " 
          <<  player->getCol()  << " "
          <<  player->get_dir() << " "
          << player->protokolState() << " ";
    }
    
    i=0;
    for(Guard* x:guards){
      i++;
      msg << "H" << " " 
          <<  i  << " " 
          <<  x->getRow()  << " " 
          <<  x->getCol()  << " "
          << x->get_dir()<< " ";
    }
    
    cli->send(msg.str());
  }
  cli_lock.unlock();
}



/**
 * @brief Spousti v nekonecne smycce Room::loop
 * 
 * Slouzi pro vytvoreni vytvoreni noveho vlakna nad objetktem Room
 * Pouziti std::thread t (roomLoop,ptr_room); t.detach();
 * @param x Ukazatel na Room nebo zdedeny objekt
 */
void roomLoop(Room *x)
{
  x->setRun(true);
  while(x->loop()){
  }
  x->setRun(false);
}
