/**
 * @brief Třídy pro místnosti (obsluhu klientů)
 * 
 * @author Vítězlsav Kříž - xkrizv01
 * @file room.hpp
 */

#ifndef ROOM_HPP
#define ROOM_HPP

#include "client.hpp"
#include "servermaze.hpp"
#include "playerlogic.hpp"
#include "guard.hpp"
#include <string>
#include <map>
#include <fstream>
#include <list>
#include <mutex>
#include <memory>
#include <ctime>


/**
 * @brief Místnost pro klienty
 * 
 * Poskytuje prostor pro klienty, umožňuje připojeni, odpojení,
 * zasílaní zpráv.
 * Použivá zámky pro bezpečný přístup ve vláknech
 * Metoda Room::loop, je určena pro spouštění ve smyčce ve vlastním vlákně
 * Toto spouštění umožňuje funkce @ref roomLoop
 * Dále se od ní dědí. V programu není přímo využita.
 */
class Room{
  friend void roomLoop(Room *x);
protected:
  std::recursive_mutex cli_lock; ///< Mutex pro seznam klientů

  std::list<Client *> clients; ///< Seznam připojených klientů
  int delay=100; ///< Zpoždění v milisekundach mezi spouštěním Room::loop;
  void sendAll(std::string msg);
  virtual bool loop();
  
  std::mutex stop_lock; ///< Mutex pro přístup k @ref run a @ref prepare_for_stop
  bool prepare_for_stop=false; ///< Příkaz k ukončení běhu @ref loop
  bool run=true; ///< Signalizace ukončení běhu, nastavuje @ref roomLoop
  
  inline void setRun(bool set);
  inline void doStop();
  inline bool checkStop();
  
public:
  Room() {}
  virtual ~Room();
  
  inline bool canRemove();
  

  virtual void connectClient(Client * cli);

};

/**
 * @brief Setter pro run
 * 
 * - Používá zámky pro výlučný přístup
 * - Využito v @ref roomLoop
 * @param set Nastavovana hodnota, true = běží
 */
void Room::setRun(bool set) {
  stop_lock.lock();
  run=set;
  stop_lock.unlock();
}

/**
 * @brief Zastaví běh @ref loop
 */
void Room::doStop() {
  stop_lock.lock();
  prepare_for_stop=true;
  stop_lock.unlock();
}

/**
 * @brief Kontrola zda se má zastavit
 * @return true = stop
 */
bool Room::checkStop(){
  bool ret;
  stop_lock.lock();
  ret=prepare_for_stop;
  stop_lock.unlock();
  return ret; 
}

/**
 * @brief Kontrola zda je již cyklus zastaven a je možné objekt odstranit
 * @return true = lze odstranit
 */
bool Room::canRemove() { 
  bool ret;
  stop_lock.lock();
  ret= !run;
  stop_lock.unlock();
  return ret; 
}


/**
 * @brief Místnost s rozehranou hrou
 */
class GameRoom: public Room{
  
  ServerMaze plan; ///< Hra, bludiště na kterém se hraje
    
  unsigned maxPlayers=4; ///< Maximaní možný počet hráčů
  
  std::map<int,unsigned> client_player; ///< Vazba mezi @ref clients a @ref players
  std::map<unsigned,PlayerLogic*> players; ///< Hráči v bludišti
  std::vector<Guard*> guards; ///< Strážní v bludišti
  
  std::list<Client*>::iterator doOneClient(std::list<Client*>::iterator it);
  
  bool loop();
  
  void spawnGuards();
  bool gameEnded=false;
  void endGame();
   time_t startTime;
public:  
  std::string name; ///< Jmeno hry
public:
  /**
   * @brief Vytvoří hru podle parametrů
   * @param x_plan  Mapa
   * @param x_delay Zpoždění jednoho kroku
   * @param x_name  Jméno hry
   * @param x_maxP  Maximální počet hráčů
   */
  GameRoom(ServerMaze x_plan,double x_delay,std::string x_name,unsigned x_maxP):
    plan(x_plan),maxPlayers(x_maxP),name(x_name)
  {
    time_t tt;
    std::time(&tt);
    std::srand(tt);
    
    delay=x_delay*1000;
    std::time(&startTime);
    spawnGuards();
  }
  
  ~GameRoom();
  
  void connectClient(Client * cli);
  
  /** @return Aktuální počet hráčů */
  int countP() {return players.size();}
  /** @return Maximální počet hráčů */
  int maxP() {return maxPlayers;}

};





/**
 * @brief Singleton - Vstupni mistnost pro obsluhu nehrajicich klientu
 * 
 * Dedi od Room, menis e na singleton
 * Obsluhuje hrace, kteri nejsou ve hre.
 * Posila jim informace o aktualnich hrach, mapach, ....
 * Pri spusteni serveru nacita vsechn mapy do pameti, pak uz ne.
 */
class LobbyRoom: public Room{
  
  
  std::vector<GameRoom *> games;
  bool loop();
  
  std::vector<std::pair<std::string,ServerMaze>> maps;
  
  GameRoom* createGame(int id_map,double delay,std::string name,int max_players);
  std::vector<GameRoom *> toRemove;
  
  LobbyRoom();
  ~LobbyRoom();
 
public:
  
  static LobbyRoom& GetInstance()
  {
      static LobbyRoom instance;
      return instance;
  }
  void addGame(GameRoom *game);
  void connectClient(Client * cli);
};

void roomLoop(Room *x);


#endif // ROOM_HPP
