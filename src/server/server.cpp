/**
 * @brief Main funkce pro server
 * 
 * Čeká na příchozí spojení od klientů, po navázání spojení
 * předává klienta druhému vláknu.
 * @file server.cpp
 * @author Vítězlsav Kříž - xkrizv01
 */


#include <ctime>
#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <cstdlib>
#include <vector>
#include "servermaze.hpp"
#include "room.hpp"
#include "client.hpp"
#include <thread>
#include <memory>
#include <utility>


using boost::asio::ip::tcp;

int main(int argc,char**argv){
  
  try{
    boost::asio::io_service io_service;   
    int port=45678; ///< port serveru, výchozí hodnota
    
    if(argc==2){//zpracování jediného parametru
      port=std::atoi(argv[1]);
    }
    
    
    tcp::endpoint endpoint(tcp::v4(),port);
    tcp::acceptor acceptor(io_service, endpoint);
    
    // start vlakna pro vstupni/obsluhujici mistnost
    std::thread t (roomLoop,&LobbyRoom::GetInstance());
    t.detach();
    
    
    while (true)
    {
      boost::system::error_code ec;
      
      Client * cli ( new Client(io_service) );
      //cekani na klienta
      acceptor.accept(cli->sock, ec);
      if (!ec)
      {
        
        LobbyRoom::GetInstance().connectClient(cli);
      }
    }
    
  }
  catch (std::exception& e){
    std::cerr << e.what() << std::endl;
  }
  catch (const char *e){
    std::cerr << "E:" << e << std::endl;
  }
  
  return 0;
}
