TEMPLATE = app

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QT= 

include(../icp.pri)


HEADERS = client.hpp \
          room.hpp \
          ../shared/maze.hpp \
    guard.hpp \
    playerlogic.hpp \
    ../shared/abstractplayer.hpp \
    servermaze.hpp

SOURCES = client.cpp \
          room.cpp \
          server.cpp \
    guard.cpp \
    playerlogic.cpp \
    servermaze.cpp
          

LIBS += ../shared/libmaze.a -lboost_system -pthread

TARGET = server

PRE_TARGETDEPS += ../shared/libmaze.a
