/**
 * @brief Třída pro bludiště na straně serveru
 * 
 * @file servermaze.cpp
 * @author Vítězlsav Kříž - xkrizv01
 */

#include "servermaze.hpp"
#include <iostream>
#include <fstream>


/**
 * @brief Načte bludiště ze souboru
 * @param filename Jméno souboru s bludištěm
 */
ServerMaze::ServerMaze( const char* filename )
{
  using namespace std;
  string line;
  ifstream file ( filename);
  if ( file.is_open() )
  {
    if(file >> R >> C){
      getline(file,line);
      for(int i=0;i<R;++i){
        int j=0;
        getline(file,line);
        for(char &c:line){
          Object::t_type type;
          switch(c){
            case '#':
              type=Object::WALL;
              break;
            case ' ':
              type=Object::EMPTY;
              break;
            case '+':
              type=Object::KEY;
              break;
            case 'G':
              type=Object::GATE;
              break;
            case 'C':
              type=Object::GOAL;
              break;
            case 'S':
              player_spawns.push_back(spawn(i,j));
              type=Object::EMPTY;
              break;
            case '1':
            case '2':
            case '3':
            case '4':  
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
              guard_spawns.push_back(spawn(i,j,c-'0'));
            default:
              type=Object::EMPTY;
              break;
          }
          j++;
          data.push_back(Object(type));
        }
        if(j!=C)
          throw("BAD FORMAT");
      }
    }
    
    file.close();
  }
  else throw("Opening file");
}