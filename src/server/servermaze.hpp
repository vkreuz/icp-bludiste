/**
 * @brief Třída pro bludiště na straně serveru
 * 
 * @file servermaze.hpp
 * @author Vítězlsav Kříž - xkrizv01
 */
#ifndef SERVERMAZE_HPP
#define SERVERMAZE_HPP
#include "../shared/maze.hpp"


/**
 * @brief Bludiště na straně serveru.
 * 
 * Obsahuje navíc informace, které nejsou potřebné u klienta
 */
class ServerMaze : public Maze
{
  
public:
  ServerMaze() {}
  ServerMaze( const char* filename );
  
  
  /**
   * @brief Pozice, kde se má umístit hráč/hlídač
   */
  struct spawn{
    int row; ///< řádek v bludišti
    int col; ///< sloupec v bludišti
    int level; ///< úroveň hlídače
    spawn(int r,int c,int l): row(r),col(c),level(l) {}
    spawn(int r,int c): row(r),col(c),level(0) {}
  };
  
  /// Nastaveno na true, pokud hra skončila.
  /** Hra končí při dosažení cílového políčka hráčem */
  bool end=false;
  
  std::queue<std::pair<int,int>> changes; ///< Změny v bludišti na pozicích row,col
  std::vector<spawn> player_spawns; ///<Souřadnice pro umísťování hráčů
  std::vector<spawn> guard_spawns; ///<Souřadnice pro prvotní umísteění hlídačů
};

#endif // SERVERMAZE_HPP
