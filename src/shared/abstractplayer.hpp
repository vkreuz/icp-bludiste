/**
 * @brief Třída pro hráče v bludišti
 * 
 * Původní myšlenka této třídy byla pro využití jak v klientovy tak na serveru
 * @file abstractplayer.hpp
 * @author Vítězlsav Kříž - xkrizv01
 */
#ifndef ABSTRACTPLAYER_HPP
#define ABSTRACTPLAYER_HPP

/**
 * @brief Abstraktni trida se spolecnym rozhranim pro hrace
 */
class AbstractPlayer
{
public:
  /** Typ pro smer otoceni hrace */
  enum t_direction {
    N=0, ///< Sever
    E=1, ///< Vychod
    S=2, ///< Jih
    W=3, ///< Zapad
  };
protected:
  t_direction direction=N; ///< smer kterym je hrac otoceny
  int row,col;

public:
  t_direction get_dir() const{
    return direction;
  }

  int getRow() const { return row; }
  int getCol() const { return col; }
};

#endif // ABSTRACTPLAYER_HPP
