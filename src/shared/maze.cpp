/**
 * @brief Tridy pro zobrazemi bludiste
 * 
 * @author Vítězlsav Kříž - xkrizv01
 * @file maze.cpp
 */


#include <fstream>
#include <string>
#include "maze.hpp"
#include <iostream>
#include <vector>
#include <sstream>



/**
 * @brief Obsazeni policka hracem
 * @param player hrac, ktery na policko vstupuje
 */
void Object::enter(AbstractPlayer *player){
  if (host!=nullptr)
    throw("Cannot enter");
  host=player;
}

/**
 * @brief Opusteni policka hracem
 * @param player hrac, ktery policko opousti
 */
void Object::leave(AbstractPlayer *player){
  if (host!=player)
    throw("Bad leave");
  host=nullptr;
}



/**
 * @brief Konstrukor na zaklade souvisleho stringu
 * 
 * Vyuziti pri prijmu ze serveru
 * @param rows Pocet radku bludiste
 * @param cols Pocet sloupcu
 * @param str Souvisly string, bez koncu radku
 */
Maze::Maze(int rows, int cols, std::string str)
{
  R=rows;
  C=cols;
  int i=0;
  for(char &c : str){
    i++;
    switch (c){
      case '.':
        data.push_back( Object(Object::EMPTY));
      break;
      case 'O':
        data.push_back( Object(Object::GATE));
        data.back().action(); //opengate
      break;
      case 'C':
        data.push_back( Object(Object::GATE));        
      break;
      case '+':
        data.push_back( Object(Object::KEY));        
      break;      
      case 'G':
        data.push_back( Object(Object::GOAL));        
      break;
      case '#':
        data.push_back( Object(Object::WALL));        
      break;
      default:
        data.push_back( Object(Object::EMPTY));
    }
  }
  if(i!=(R*C)){
    //chyba
  }
  
}

/**
 * @brief Prevede bludiste na souvisly string
 * 
 * Vyuziti pri odesilani serverem
 * @return bludiste jako string
 */
std::string Maze::toString()
{
  std::string result;
  for(int i = 0; i< R; ++i){
    for(int j=0;j<C;++j){
      switch (getField(i,j).get_type()){
        case Object::EMPTY:
          result+= ".";
        break;
        case Object::GATE:
          if (getField(i,j).is_open())
            result+=  "O";
          else
            result+=  "C";
        break;
        case Object::KEY:
          result+= "+";
        break;
        case Object::GOAL:
          result+= "G";
        break;
        case Object::WALL:
          result+=  "#";
        break;
      }
    }
  }
  return result;
}


/**
 * @brief Vytiskne bludiste na cout
 * 
 * Debugovaci funkce
 */
void Maze::Print() 
{
  std::ostringstream str;
  for(int i = 0; i< R; ++i){
    for(int j=0;j<C;++j){
      
      if (getField(i,j).is_occupied()){
        
          switch (getField(i,j).get_host()->get_dir()){
            case AbstractPlayer::N: str << "^"; break;
            case AbstractPlayer::E: str << ">"; break;
            case AbstractPlayer::S: str << "v"; break;
            case AbstractPlayer::W: str << "<"; break;
          }
      } else {
      
        switch (getField(i,j).get_type()){
          case Object::EMPTY:
            str << " ";
            break;
          case Object::GATE:
            if (getField(i,j).is_open())
              str << ".";
            else
              str << "G";
            break;
          case Object::KEY:
            str << "+";
            break;
          case Object::GOAL:
            str << "@";
            break;
          case Object::WALL:
            str << "#";
            break;
        }
      }
    }
    
    str << std::endl;
  }

  std::cout << str.str();
}


