/**
 * @brief Tridy pro zobrazemi bludiste
 * 
 * @author Vítězlsav Kříž - xkrizv01
 * @file maze.hpp
 */


#ifndef MAZE_HPP
#define MAZE_HPP
#include <vector>
#include <iostream>
#include <queue>

#include "abstractplayer.hpp"


/**
 * @brief Zakladni stavebni prvek bludiste.
 *
 * Uchovává si informaci o hráči, který na něm stojí.
 */
class Object
{
public:
  /** Typ pro urceni typu objektu */
  typedef enum {
    EMPTY, ///< Prazdne policko
    GATE,  ///< Brana, zavrena i otevrena
    KEY,   ///< Klic, pri zvednuti prejde na EMPTY
    WALL,  ///< Zed, nejde na ni vstoupit
    GOAL,  ///< Cil, konec hry
    } t_type;
private:
  bool open=false; ///< Brana otevrena/zavrena
  t_type type;///< typ objektu
  AbstractPlayer *host=nullptr; ///< Hráč/hlídač umístěný na bludišti

public:


  Object(t_type what): type(what) {} 
  
  
  t_type get_type() const { return type; }
  bool is_open() const {return open;}
  inline bool can_enter() const ;
  void enter(AbstractPlayer *player);
  void leave(AbstractPlayer *player); 
  inline bool is_occupied() const;
  inline AbstractPlayer *get_host();
  inline const AbstractPlayer *get_host() const;
  inline bool action();
  inline bool placeKey();
};


/**
 * @brief Singleton objekt zdi
 * 
 * Urceno pro vsechny prvky bludiste, kterou jsou mimo rozmery
 */
class ObjectWall:public Object{
public:
    static ObjectWall& GetInstance()
    {
        static ObjectWall instance;
        return instance;
    }
 
private:
    ObjectWall(): Object(Object::WALL) {}
    ObjectWall(const ObjectWall&);
    ObjectWall& operator=(const ObjectWall&);
};

/**
 * @brief Predikat zda jde na policko vstoupit
 * 
 * @return true = lze vstoupit
 */
bool Object::can_enter() const{
  if ( (type == EMPTY) ||
       (type == GATE && open==true) ||
       (type == GOAL)
     ) 
    if (host == nullptr)
      return true;
    else
      return false;
  else
    return false;
}

/**
 * @brief Predikát zda je někdo na políčku
 * @return true = někdo tam je
 */
bool Object::is_occupied() const {
  return host!=nullptr;
}

/**
 * @brief Získá ukazatel na toho kdo stojí na políčku
 * @return Ukazatel na hráče hlídače
 */
AbstractPlayer *Object::get_host() {
  return host;
}

/**
 * @brief Získá ukazatel na toho kdo stojí na políčku
 * @return Ukazatel na hráče hlídače
 */
const AbstractPlayer *Object::get_host() const{
  return host;
}

/**
 * @brief Provedení specifické akce pro daný typ políčka
 * 
 * Pro klíč jej zvedne, pro bránu otevře.
 * @return Potvrzení úspěchu, true = úspěch
 */
bool Object::action() {
  if (type == KEY){
    type = EMPTY;
    return true;
  }
  if (type == GATE && open==false){
    open=true;
    return true;
  }
  return false;
}


/**
 * @brief Umísttí klíč na políčko
 * @return Potvrzení úspěchu, true = úspěch 
 */
bool Object::placeKey()
{
  if( type ==EMPTY){
    type=KEY;
    return true;
  }
  else
    return false;
}


/**
 * @brief Trida pro bludiste
 *
 * Udrzuje v sobe vsechny stavebni objekty
 *
 */
class Maze
{
protected:  
  int R; ///< rows
  int C; ///< columns
  std::vector<Object> data; ///< Ulozene objekty

public:
    
  Maze(): R(0), C(0) {}
  Maze(int rows, int cols, std::string str);

  inline Object & getField(int row, int col);
  const Object & getField(int row, int col) const {
    return getField(row,col);
  }
  void Print(); 
  
  std::string toString();

  int Rows() { return R; }
  int Cols() { return C; }
  std::vector<Object>* getMaze() { return &data; }
};





/**
 * @brief Vrati stavebni objekt na dane pozici
 *
 * @param col Souradnice sloupce - osa x
 * @param row Souradnice radku - osa y
 * @return Reference na stavebni objekt
 */
Object & Maze::getField(int row,int col)
{
  if(col>=C || row >=R || col<0 || row <0)
    return ObjectWall::GetInstance();
  
  return data[row*C+col];

}



///**
// * Hrac, Umoznuje menit pozici na bludisti
// * Pro klienta, urceno ke zdedeni pro vice variant (grafika,terminal)
// * Nevyuzito
// */
//class Player : public AbstractPlayer{
//  Maze & game;
//public:
//  Player(Maze & g,int r, int c): game(g) {
//    row=r;
//    col=c;
//    game.getField(r,c).enter(this);
//  }
//  Player(Maze & g,int r, int c,t_direction dir): game(g) {
//    row=r;
//    col=c;
//    direction=dir;
//    game.getField(r,c).enter(this);
//  }
  
//  void set_position(int set_row, int set_col){
//    game.getField(row,col).leave(this);
//    row=set_row;
//    col=set_col;
//    game.getField(row,col).enter(this);
//  }
  
//  void set_position(int set_row, int set_col, t_direction set_dir){
//    direction=set_dir;
//    set_position(set_row,set_col);
//  }

//};


#endif
