TEMPLATE = lib

CONFIG += staticlib

CONFIG -= qt
QT       -= core gui


include(../icp.pri)

HEADERS = maze.hpp \
    abstractplayer.hpp

SOURCES = maze.cpp

TARGET = maze
